FROM node:12-alpine as node
WORKDIR /usr/src/app
COPY package.json ./
RUN npm install
COPY jsconfig.json .
COPY src /usr/src/app/src
COPY public ./public
RUN npm run build

FROM nginx:alpine as server
COPY --from=node /usr/src/app/build/ /usr/share/nginx/html/
COPY *.conf /etc/nginx/conf.d/
CMD ["nginx", "-g", "daemon off;"]

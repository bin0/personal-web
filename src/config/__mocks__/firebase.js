const firebaseMock = {
  auth: () => ({
    apiKey: 'AIzaSyCXmTetg4x1D_3JixO8X6BpJe8iQrCqijw',
    appName: '[DEFAULT]',
    authDomain: 'adolfo-onrubia-trading-app.firebaseapp.com',
    currentUser: null,
    onAuthStateChanged(callback) {
      callback({ providerData: [{ user: 'test' }] });
    },
    signOut() {},
  }),
};

export default firebaseMock;

const utils = (locale = 'es-ES') =>
  new Intl.NumberFormat(locale, {
    style: 'currency',
    currency: 'EUR',
    minimumFractionDigits: 0,
  });

export default utils;

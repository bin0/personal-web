export const capitalize = (str) => {
  if (typeof str !== 'string') {
    throw new Error(`Only strings can be capitalized, received ${typeof str}`);
  }

  if (str.length === 0) {
    throw new Error("Empty strings can't be capitalized");
  }

  if (str.length < 2) {
    return str.toUpperCase();
  }
  const firstChar = str.charAt(0).toUpperCase();

  return `${firstChar}${str.slice(1, str.length)}`;
};

export const getLink = (list, route, index) => {
  let res = '/';

  if (index === 0) {
    res += route;
  } else if (index === 1) {
    res += `${list[index - 1]}/${route}`;
  }

  return res;
};

export const getBreadcrumbFromPathname = (pathname) => {
  if (typeof pathname !== 'string') {
    throw new Error('argument must be a string');
  } else if (pathname.charAt(0) === '/') {
    const path = pathname.split('/');

    return path.slice(1, path.length);
  }

  return pathname;
};

export const formatCurrency = (locale, value, minimumFractionDigits = 2) =>
  Intl.NumberFormat(locale, {
    maximumFractionDigits: 8,
    minimumFractionDigits,
  }).format(value);

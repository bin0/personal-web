export const parseMarketPriceFromObj = (marketData, market) => {
  const BID = parseFloat(marketData.getValue('BID'));
  const OFFER = parseFloat(marketData.getValue('OFFER'));
  const CURRENT = Number(parseFloat((BID + OFFER) / 2, 10).toFixed(2));

  return {
    [market]: {
      BID,
      OFFER,
      CURRENT,
    },
  };
};

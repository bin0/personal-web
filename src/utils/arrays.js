export const isLastItem = (list, index) =>
  index > -1 && index < list.length && index === list.length - 1;

/**
 * @description Fills an array with strings from `fields` in an object
 *
 * @param  {...string} fields
 * @param {[], Object}
 *
 * @example
 * const example = [{ foo: 'bar', list: ['foo', 'bar']}];
 * example.reduce(reduceListByFields('foo', 'list));
 *
 * @returns ['bar', 'foo', 'bar']
 */
export const reduceListByFields =
  (...fields) =>
  (prev, next) =>
    prev.concat(...fields.map((field) => next[field]));

/**
 *
 * @param {String} field property field name (key)
 */
export const orderObjectListByField = (field) => (a, b) => {
  if (a[field] < b[field]) {
    return -1;
  }
  if (a[field] > b[field]) {
    return 1;
  }
  return 0;
};

export const findById = (id) => (item) => item.id === id;
export const findBySymbol = (symbol) => (item) => item.symbol === symbol;

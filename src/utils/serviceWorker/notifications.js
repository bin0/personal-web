import * as serviceWorker from '.';
// import { actions } from 'reducers/app';

const worker = (dispatch) => {
  // const config = {
  //   onUpdate(registration) {
  //     console.log('onUpdate registration', registration);
  //     setTimeout(() => {
  //       dispatch(actions.onUpdate(registration));
  //     }, 5000);
  //   },
  //   onSuccess(registration) {
  //     dispatch(actions.onRegister());
  //     console.log('onSuccess registration', registration);
  //   },
  // };
  // serviceWorker.register(config);
  serviceWorker.unregister();
};

export default worker;

export const isFirstVisit = (callback) => {
  const session = sessionStorage.getItem('ao-first-visit');

  if (!session) {
    sessionStorage.setItem('ao-first-visit', true);

    callback();
  }
};

import moment from 'config/moment';

export const currentMonthText = (locale = 'es-ES') => {
  const options = { month: 'long' };

  return new Intl.DateTimeFormat(locale, options).format(Date.now());
};

export const currentYear = new Date().getFullYear();

export const customDate = (date) =>
  moment(date).format('[Abierta el] DD/MM/YYYY [a las] HH:MM:SS');

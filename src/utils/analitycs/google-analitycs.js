import ReactGA from 'react-ga';

class ReactAnalitycs {
  constructor() {
    this.ReactGA = ReactGA;

    this.isActive = process.env.REACT_APP_GA_ACTIVE;
    this.initializeAnalitycs();
  }

  initializeAnalitycs() {
    let userId = localStorage.getItem('userId');
    if (!userId) {
      userId = 'Invitado';
    }

    if (process.env.REACT_APP_GA_ID && this.isActive) {
      if (process.env.NODE_ENV !== 'production') {
        this.ReactGA.initialize(process.env.REACT_APP_GA_ID, {
          debug: true,
        });
      } else {
        this.ReactGA.initialize(process.env.REACT_APP_GA_ID, {
          gaOptions: { userId },
        });
      }
    } else if (!!!this.isActive) {
      throw new Error('Se require el ID de GA');
    }
  }

  pageview(route, name) {
    if (this.isActive) {
      this.ReactGA.pageview(route, [], name);
    }
  }

  event(props) {
    if (this.isActive) {
      this.ReactGA.event({ ...props });
    }
  }
}

export default new ReactAnalitycs();

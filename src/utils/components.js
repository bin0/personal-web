import React, { Suspense } from 'react';
import LoadingBar from 'react-redux-loading-bar';

export const renderLazyComponent = (WrappedComponent, props) => (
  <Suspense
    fallback={<LoadingBar className="redux-loading-bar" showFastActions />}
  >
    <WrappedComponent {...props} />
  </Suspense>
);

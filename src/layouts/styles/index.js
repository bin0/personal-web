import styled from 'styled-components';

export const MainLayoutStyled = styled.div`
  /* position: absolute;
  top: 0;
  left: 0;
  width: 100vw;
  height: 100vh;
  margin: 0;
  padding: 0;
  box-sizing: border-box; */
`;

export const DeveloperLayoutStyled = styled.div``;

export const DeveloperContentStyled = styled.div`
  max-width: 1200px;
  margin: 0 auto;
  ul {
    list-style: square;
    margin-left: 1rem;

    li {
      margin-left: 1rem ul > li {
        font-size: 0.8rem;
      }
    }
  }

  .app-logo-container {
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    height: 400px;
    background-color: #282c34;
    font-size: calc(10px + 2vmin);
    color: white;

    h4 {
      color: #61dafb;
      text-shadow: 1px 1px #2b6474;
      font-weight: 100;
      font-size: 1.7rem;

      @media (max-width: 768px) {
        font-size: 1.6rem;
      }
    }

    p {
      color: #ccc;
      font-size: 0.7rem;
      text-transform: uppercase;
      letter-spacing: 0.45rem;
      font-weight: 600;
    }
  }
  footer {
    position: absolute;
    bottom: 1rem;
    right: 1rem;
  }
`;

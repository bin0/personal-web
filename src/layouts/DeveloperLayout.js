import React from 'react';
import { LoadingBar } from 'react-redux-loading-bar';
import NavMenu from '../components/NavMenu';
import HomeFooter from '../components/HomeFooter';
import { DeveloperLayoutStyled, DeveloperContentStyled } from './styles';
import AppBar from '../components/AppBar';

const DeveloperLayout = ({ children }) => (
  <DeveloperLayoutStyled>
    <AppBar
      WithMenu={NavMenu}
      title="Adolfo Onrubia"
      subtitle="Senior FullStack Developer"
      color="primary"
    />
    <LoadingBar showFastActions />

    <DeveloperContentStyled>{children}</DeveloperContentStyled>

    <HomeFooter />
  </DeveloperLayoutStyled>
);

export default DeveloperLayout;

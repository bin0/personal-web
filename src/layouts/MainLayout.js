import React from 'react';
import PropTypes from 'prop-types';
import { LoadingBar } from 'react-redux-loading-bar';
import NavMenu from 'components/NavMenu';
import HomeFooter from 'components/HomeFooter';
import AppBar from 'components/AppBar';
import { MainLayoutStyled } from './styles';

const MainLayout = ({ children, history, ...props }) => {
  return (
    <MainLayoutStyled>
      <LoadingBar showFastActions />

      <AppBar
        title="Kaizen"
        subtitle="Hoy mejor que ayer, mañana mejor que hoy"
        WithMenu={NavMenu}
        history={history}
      />

      {children}

      <HomeFooter />
    </MainLayoutStyled>
  );
};

MainLayout.propTypes = {
  children: PropTypes.element.isRequired,
};

export default MainLayout;

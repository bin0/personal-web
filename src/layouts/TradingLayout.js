import React from 'react';
import { LoadingBar } from 'react-redux-loading-bar';
import { connect } from 'react-redux';
import Modals from 'components/Modal';
import AppBar from 'components/AppBar';
import Drawers from 'components/Drawer';
import Footer from 'components/Footer';
import Navbar from 'components/NavMenu';
import { actions as userActions } from 'reducers/user';
import { actions as authActions } from 'reducers/auth';

const TradingLayout = ({ children, ...props }) => (
  <div>
    <LoadingBar showFastActions />
    <Modals />
    <Drawers />
    <AppBar
      {...props}
      color="primary"
      title="Adolfo Onrubia"
      subtitle="Trading"
      WithMenu={Navbar}
    />
    <div className="App">{children}</div>
    <Footer />
  </div>
);

const mapDispatchToProps = { ...userActions, ...authActions };

export default connect(null, mapDispatchToProps)(TradingLayout);

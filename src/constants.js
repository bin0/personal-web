export const ALREADY_JOINED = 'already-joined';
export const ENTRYPOINT = 'root';
export const LOGO = 'logo';
export const FALLBACK = 'fallback';
export const BUTTON = 'button';
export const DIV = 'div';

import React from 'react';
import { Provider } from 'react-redux';
import { BrowserRouter as Router } from 'react-router-dom';
import { showLoading } from 'react-redux-loading-bar';
import { ThemeProvider } from '@mui/material/styles';
import routesBoot from 'routes/routesBoot';
import createStore from 'redux/create-store';
import serviceWorker from 'utils/serviceWorker/notifications';
import theme from 'styles/theme';
import Snackbar from 'components/Snackbar';
import 'utils/analitycs/google-analitycs';
import './App.scss';

const store = createStore();
serviceWorker(store.dispatch);

//! FIXME Adolfo Force show loading bar, check to use hideLoading somewhere
store.dispatch(showLoading());

const script = (
  <script
    defer
    data-project-id="13661857"
    data-project-path="onrubia78/personal-web"
    data-mr-url="https://gitlab.com"
    id="review-app-toolbar-script"
    src="https://gitlab.com/assets/webpack/visual_review_toolbar.js"
  />
);

const App = () => (
  <Provider store={store}>
    {process.env.REACT_APP_ENVIRONMENT === 'staging' && script}
    <ThemeProvider theme={theme}>
      <Router>{routesBoot}</Router>
      <Snackbar />
    </ThemeProvider>
  </Provider>
);

export default App;

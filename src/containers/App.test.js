import React from 'react';
import { MemoryRouter as Router } from 'react-router-dom';
import { shallow } from 'enzyme';
import toJSON from 'enzyme-to-json';

import App from './App';

describe('<App />', () => {
  it('should be mounted', () => {
    const shallowWrapper = shallow(
      <Router initialEntries={[{ pathname: '/', key: 'test' }]}>
        <App />
      </Router>
    );

    expect(toJSON(shallowWrapper)).toMatchSnapshot();
  });
});

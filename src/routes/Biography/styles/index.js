const styles = (theme) => ({
  biography: {
    margin: '0 2rem',
    textAlign: 'justify',
  },
});

export default styles;

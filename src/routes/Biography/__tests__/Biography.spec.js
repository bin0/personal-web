import React from 'react';
import { mount } from 'enzyme';
import toJSON from 'enzyme-to-json';
import Biography from '../components/BiographyView';

describe('<Biography />', () => {
  describe('when it runs', () => {
    const props = {
      classes: {
        biography: 'test-class',
      },
    };
    it('should be mounted', () => {
      const mountWrapper = mount(<Biography {...props} />);

      expect(toJSON(mountWrapper)).toMatchSnapshot();
    });
  });
});

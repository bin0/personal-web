import React, { createElement } from 'react';
import PropTypes from 'prop-types';
import { Route } from 'react-router-dom';
import { renderLazyComponent } from '../utils/components';

const RouteWithLayout = ({ layout, component, ...routeProps }) => {
  const ViewComponent = (props) =>
    createElement(layout, props, createElement(component, props));

  return (
    <Route
      {...routeProps}
      render={(props) => renderLazyComponent(ViewComponent, props)}
    />
  );
};

RouteWithLayout.propTypes = {
  layout: PropTypes.any.isRequired,
  component: PropTypes.oneOfType([PropTypes.object, PropTypes.func]).isRequired,
};

export default RouteWithLayout;

import React from 'react';
import { PortfolioContainer } from '../styles/portfolio';
import PortfolioGrid from './PortfolioGrid';

const settings = {
  'grid-gap': false,
  'grid-template-columns': false,
};

const PortfolioView = () => {
  return (
    <PortfolioContainer selected>
      <PortfolioGrid settings={settings} />
    </PortfolioContainer>
  );
};

export default PortfolioView;

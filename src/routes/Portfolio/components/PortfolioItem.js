import React, { useState } from 'react';

const PortfolioItem = () => {
  const [expanded, setExpanded] = useState(false);

  return (
    <div
      className={`card card--4 ${expanded ? 'card--expanded' : ''}`}
      onClick={() => {
        setExpanded((state) => !state);
      }}
    >
      <div>
        <div className="card__avatar" />
        <div className="card__title" />
        <div className="card__description" />
      </div>
    </div>
  );
};

export default PortfolioItem;

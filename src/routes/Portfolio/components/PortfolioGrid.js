import React, { Component } from 'react';
import { wrapGrid } from 'animate-css-grid';
import Item from './PortfolioItem';
import '../styles/portfolio.scss';

class PortfolioGrid extends Component {
  componentDidMount() {
    // will automatically clean itself up when dom node is removed
    wrapGrid(this.grid, {
      easing: 'backOut',
      stagger: 10,
      duration: 400,
    });
  }

  render() {
    let classes = 'grid';
    // Object.keys(this.props.settings)
    //   .filter((k) => this.props.settings[k])
    //   .forEach((k) => (classes += " " + k));
    return (
      <div className={classes} ref={(el) => (this.grid = el)}>
        {[1, 2, 3, 4, 5, 6, 7, 8, 9].map((i) => (
          <Item key={i} />
        ))}
      </div>
    );
  }
}

export default PortfolioGrid;

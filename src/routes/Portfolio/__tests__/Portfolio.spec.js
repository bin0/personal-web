import React from 'react';
import { mount } from 'enzyme';
import toJSON from 'enzyme-to-json';

import Portfolio from '../components/PortfolioView';
// import createStore from 'redux/create-store';

describe('<Portfolio />', () => {
  describe('when it runs', () => {
    it('should be mounted', () => {
      // const store = createStore();
      const mountWrapper = mount(<Portfolio />);

      expect(toJSON(mountWrapper)).toMatchSnapshot();
    });
  });
});

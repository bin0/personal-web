import React from 'react';
import { Provider } from 'react-redux';
import { mount } from 'enzyme';
import toJSON from 'enzyme-to-json';
import TradingView from '../components/TradingView';
import configureStore from 'redux-mock-store';
// import createStore from 'redux/create-store';

const mockStore = configureStore([]);
jest.mock('utils/dates', () => ({
  currentMonthText: jest.fn().mockReturnValue('April'),
}));

describe('<TradingView />', () => {
  describe('when it runs', () => {
    const props = {
      authenticated: false,
      checkUser: jest.fn(),
      newPosition: {},
      selectedMarket: 'DOW',
    };
    it('should be mounted', () => {
      const store = mockStore({});
      const mountWrapper = mount(
        <Provider store={store}>
          <TradingView {...props} />
        </Provider>
      );

      expect(toJSON(mountWrapper)).toMatchSnapshot();
    });
  });

  // describe('when is not authenticated', () => {
  //   const props = {
  //     selectedMarket: 'DOW',
  //     newPosition: {},
  //     authenticated: false,
  //   };
  //   it('should be mounted', () => {
  //     const store = createStore();
  //     const mountWrapper = mount(
  //       <Provider store={store}>
  //         <TradingView {...props} />
  //       </Provider>
  //     );

  //     expect(toJSON(mountWrapper)).toMatchSnapshot();
  //   });
  // });
});

import { connect } from 'react-redux';
import withStyles from '@mui/styles/withStyles';
import { checkUser } from 'reducers/auth';
import { actions as modalActions } from 'reducers/modal';
import TradingView from '../components/TradingView';
import { actions as tradingActions } from '../modules/trading';
import { actions as equityActions } from '../modules/equity';
import styles from '../styles/trading';
import '../styles/trading.scss';

export const mapStateToProps = ({ trading, auth }) => ({
  selectedMarket: trading.positions.selectedMarket,
  newPosition: trading.positions.newPosition,
  authenticated: auth.authenticated,
  equity: trading.equity,
  realTrades: trading.realTrades.list,
});

export const mapDispatchToProps = {
  ...modalActions,
  ...tradingActions,
  ...equityActions,
  checkUser,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withStyles(styles)(TradingView));

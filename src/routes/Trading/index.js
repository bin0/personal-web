import TradingContainer from './containers/TradingContainer';

export { default as tradingReducer } from './modules/trading';
export { default as tradesReducer } from './modules/trades';
export { default as equityReducer } from './modules/equity';

export default TradingContainer;

import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import LoadingBar from 'react-redux-loading-bar';

const Equity = ({ equity, get }) => {
  useEffect(() => {
    get();
  }, [get]);

  if (!equity.dataLoaded) {
    return (
      <LoadingBar className="redux-loading-bar" showFastActions scope="this" />
    );
  }

  return (
    <div className="equity-container">
      <ul>
        {Object.keys(equity.data).map((field) => (
          <li key={field}>
            {field}: <b>{equity.data[field]}</b>
          </li>
        ))}
      </ul>
    </div>
  );
};

Equity.propTypes = {
  get: PropTypes.func.isRequired,
  equity: PropTypes.object.isRequired,
};

export default Equity;

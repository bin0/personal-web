import React, { Component, Suspense } from 'react';
import PropTypes from 'prop-types';
import { Typography } from '@mui/material';
import withStyles from '@mui/styles/withStyles';
import StyledFirebaseAuth from 'react-firebaseui/StyledFirebaseAuth';
import SecurityIcon from '@mui/icons-material/Security';
import literals from 'i18n/es-ES';
import Positions from './Positions';
import Trades from './Trades';
import Equity from './Equity';
import RetoTrading from './RetoTrading';
import RealTradingBalance from './RealTradingBalance';
import TradingChat from './Chat';
import styles, {
  ErrorContainer,
  TradingContent,
  // TradingBanner,
  // Banner,
  TradingSection,
  TradingViewContainer,
} from '../styles/trading';
// import bannerImg from 'assets/img/banner-trading-1280.jpg';
import firebase from 'config/firebase';
import Analitycs from 'utils/analitycs/google-analitycs';

const uiConfig = {
  signInFlow: 'popup',
  signInOptions: ['google.com'],
  callbacks: {
    signInSuccessWithAuthResult: () => false,
  },
};

class TradingView extends Component {
  componentDidMount() {
    this.props.checkUser();
    Analitycs.pageview('/trading');
    Analitycs.event({
      category: 'User',
      action: 'Navigates to trading',
      value: 1,
      label: 'Visited Trading Page',
      nonInteraction: true,
    });
    // this.props.getChartData('DOW', 100);
    // this.props.getChartData('DAX', 100);
    window.scrollTo(0, 0);
  }

  // TODO Add doc to handleOpenPosition
  handleOpenPosition = () => {
    const { selectedMarket, newPosition } = this.props;
    this.props.onOpenPosition(selectedMarket, newPosition);
    Analitycs.event({
      category: 'Trading',
      action: 'New position opened',
      value: 1,
      label: 'New position',
    });
  };

  render() {
    const { classes, authenticated, openModal, realTrades } = this.props;

    return (
      <TradingViewContainer>
        {/* <TradingBanner><Banner src={bannerImg} alt="banner" /></TradingBanner> */}
        {authenticated ? (
          <TradingContent>
            <Typography variant="h2">Mi sección de trading</Typography>
            <TradingSection>
              <RealTradingBalance realTrades={realTrades} />
            </TradingSection>
            <TradingSection>
              <h1 className={classes.h1}>{literals.TRADING.title}</h1>
              <h2 className={classes.h2}>{literals.TRADING.subtitle}</h2>
              <Positions />
            </TradingSection>
            <TradingSection>
              <Equity equity={this.props.equity} />
            </TradingSection>
            <TradingSection>
              <RetoTrading openModal={openModal} classes={classes} />
            </TradingSection>
            <TradingSection>
              <Suspense fallback={<div>Loading...</div>}>
                <Trades />
              </Suspense>
            </TradingSection>
            {/* {this.props.match.url === '/trading/trades' && <Trades />} */}
            <TradingChat />
          </TradingContent>
        ) : (
          <ErrorContainer>
            <SecurityIcon style={{ fontSize: 60 }} />
            <h1>No tienes acceso para ver esta sección</h1>
            <Typography color="secondary" paragraph>
              Accede para ver el contenido
            </Typography>
            <StyledFirebaseAuth
              uiConfig={uiConfig}
              firebaseAuth={firebase.auth()}
            />
          </ErrorContainer>
        )}
      </TradingViewContainer>
    );
  }
}

TradingView.propTypes = {
  selectedMarket: PropTypes.string.isRequired,
  newPosition: PropTypes.object.isRequired,
  authenticated: PropTypes.bool.isRequired,
  checkUser: PropTypes.func.isRequired,
};

export default withStyles(styles)(TradingView);

import React from 'react';
import PropTypes from 'prop-types';
import { Typography, Button } from '@mui/material';
import AccountBalanceIcon from '@mui/icons-material/AccountBalance';
import AccountBalanceWalletIcon from '@mui/icons-material/AccountBalanceWallet';
import { BoxTitle } from '../styles/trading';
import Separator from 'components/Separator';

const RetoTrading = ({ classes, openModal }) => {
  const handleMoreInfoClick = () => {
    openModal({ modalType: 'TRADES_MORE_INFO', modalProps: {} });
  };

  return (
    <div>
      <BoxTitle>
        <AccountBalanceIcon color="secondary" className={classes.titleIcon} />
        <h1 className={classes.h1}>Reto Real Trading 2019</h1>
      </BoxTitle>

      <Typography className={classes.h2} paragraph>
        Objetivo: conseguir 99.000€ en un año con una cuenta real de trading de
        1.000€
      </Typography>

      <BoxTitle>
        <AccountBalanceWalletIcon
          color="secondary"
          className={classes.titleIcon}
        />
        <h1 className={classes.h1}>Objetivo diario 4,48%</h1>
      </BoxTitle>
      <Typography variant="caption" color="secondary">
        Profit Objetivo: <b>23,5 pips</b>
      </Typography>
      <Typography variant="caption" color="secondary" paragraph>
        Stop Loss: <b>10 pips</b>
      </Typography>
      <Separator />
      <Button
        onClick={handleMoreInfoClick}
        variant="contained"
        color="secondary"
        size="small"
      >
        Más información
      </Button>
    </div>
  );
};

RetoTrading.propTypes = {
  classes: PropTypes.shape({
    titleIcon: PropTypes.string.isRequired,
  }),
  openModal: PropTypes.func.isRequired,
};

export default RetoTrading;

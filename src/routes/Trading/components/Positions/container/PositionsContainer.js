import { connect } from 'react-redux';
import withStyles from '@mui/styles/withStyles';

import PositionView from '../components/PositionView';
import { actions } from '../reducer/positions';
import { actions as priceActions } from '../reducer/prices';
import { actions as balanceActions } from '../reducer/balance';
import { actions as tradeActions } from '../../Trades/modules/trades';
import { actions as modalActions } from 'reducers/modal';
import styles from '../styles/positions';

export const mapStateToProps = ({ trading }) => ({
  positions: trading.positions,
  prices: trading.prices,
});

export const mapDispatchToProps = {
  ...actions,
  ...modalActions,
  ...tradeActions,
  ...priceActions,
  ...balanceActions,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withStyles(styles)(PositionView));

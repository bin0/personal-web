import React, { Component } from 'react';
import PropTypes from 'prop-types';
import CircularProgress from '@mui/material/CircularProgress';
import { CircularProgressStyled } from 'components/styles';
import Table from 'components/Table';
import { PositionsContainer, PositionContainer } from '../styles/positions';
import BalanceCards from './BalanceCards';
import { CRYPTOS } from '../../../modules/constants';

// FIXME checkout https://mui.com/components/use-media-query/#migrating-from-withwidth
const withWidth = () => (WrappedComponent) => (props) =>
  <WrappedComponent {...props} width="xs" />;

class PositionsView extends Component {
  static propTypes = {
    classes: PropTypes.object.isRequired,
    onOpenPosition: PropTypes.func.isRequired,
    prices: PropTypes.shape({
      coinbase: PropTypes.shape({
        ETH: PropTypes.object.isRequired,
      }).isRequired,
      ig: PropTypes.shape({
        DOW: PropTypes.object.isRequired,
        DAX: PropTypes.object.isRequired,
      }).isRequired,
    }).isRequired,
    positions: PropTypes.object.isRequired,
    getTrades: PropTypes.func.isRequired,
    getPositions: PropTypes.func.isRequired,
    getCoinbasePrice: PropTypes.func.isRequired,
  };

  componentDidMount() {
    this.getPositions();
    this.getPrices();
  }

  onExitPosition = (market) => {
    this.props.onExitPosition(market);
    // ReactGA.event({
    //   category: 'Trading',
    //   action: 'Close a position',
    //   value: 1,
    //   label: `Closed position on ${market}`,
    // });
  };

  getPrices = () => {
    this.props.getCoinbasePrice(CRYPTOS.ETH);
  };

  getPositions = () => {
    this.props.getPositions();
  };

  render() {
    const { positions, width } = this.props;
    const isDesktop = ['xl', 'lg', 'md'].includes(width);

    if (!positions.open.length) {
      return (
        <CircularProgressStyled>
          <CircularProgress size={60} color="secondary" thickness={1.6} />
        </CircularProgressStyled>
      );
    }
    return (
      <PositionsContainer>
        <BalanceCards positions={positions} />
        {isDesktop && (
          <PositionContainer>
            <Table
              positions={positions.open}
              onExitPosition={this.onExitPosition}
            />
          </PositionContainer>
        )}
      </PositionsContainer>
    );
  }
}

export default withWidth()(PositionsView);

import React from 'react';
import PropTypes from 'prop-types';
import withStyles from '@mui/styles/withStyles';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import TrendingDownIcon from '@mui/icons-material/TrendingDown';
import TrendingUpIcon from '@mui/icons-material/TrendingUp';
import moment from 'config/moment';
import formatter from 'utils/formatAmount';
import { MARKETS } from 'routes/Trading/modules/constants';
import styles from '../styles/balanceCards'; // GraphContent,
import { TitleContent } from '../styles/balanceCards';

class BalanceCard extends React.Component {
  static propTypes = {
    classes: PropTypes.object.isRequired,
    title: PropTypes.string.isRequired,
    market: PropTypes.string.isRequired,
    equity: PropTypes.shape({
      mediumPrice: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
      amount: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
      quantity: PropTypes.number,
    }),
    prices: PropTypes.object.isRequired,
    openModal: PropTypes.func.isRequired,
    onSelectMarket: PropTypes.func.isRequired,
  };

  handleClosePosition = (market) => {
    this.props.onSelectMarket(market);
    this.props.openModal({
      modalType: 'EXIT_POSITION',
      modalProps: { market },
    });
  };

  handleOpenPosition = (market) => {
    const { prices } = this.props;
    this.props.onSelectMarket(market);
    this.props.openModal({
      modalType: 'NEW_TRADE',
      modalProps: { market: MARKETS.IG[market], ig: prices.ig },
    });
  };

  render() {
    const { classes, title, market, equity, prices } = this.props;
    const isAdmin = Boolean(localStorage.getItem('isAdmin'));

    const cryptoPrice =
      prices.coinbase[market] && prices.coinbase[market].amount;
    const indexPrice =
      prices.ig[MARKETS.IG[market]] && prices.ig[MARKETS.IG[market]].CURRENT;
    const bull = <span className={classes.bullet}>•</span>;

    const indexSpread =
      (prices.spread && prices.spread[MARKETS.IG[market]]) || 0;

    return (
      <Card className={classes.card}>
        <CardContent>
          <TitleContent>
            <Typography
              className={classes.title}
              color="textSecondary"
              gutterBottom
            >
              {title} {bull} {market} {bull} <b>{cryptoPrice || indexPrice}</b>(
              {indexSpread})
            </Typography>
            {market === 'ETH' ? (
              <TrendingUpIcon fontSize="small" color="action" />
            ) : (
              <TrendingDownIcon fontSize="small" color="error" />
            )}
          </TitleContent>
          {equity ? (
            <>
              <Typography
                variant="h5"
                component="h2"
                color={equity.amount > 0 ? 'textPrimary' : 'error'}
              >
                {equity.amount > 0 && '+'}
                {equity.amount && formatter().format(equity.amount)}
              </Typography>
              <Typography color="textSecondary">
                {equity.direction === 'Long' && 'Comprado a '}
                {equity.direction === 'Short' && 'Vendido a '}
                {!equity.direction && 'Cerrada'}
                {equity.mediumPrice && equity.mediumPrice}{' '}
                {equity.quantity &&
                  `(${equity.quantity} contrato${
                    equity.quantity === 1 ? '' : 's'
                  })`}
              </Typography>
              <Typography className={classes.pos} variant="caption" paragraph>
                {equity.startTrade &&
                  moment(equity.startTrade).format(
                    'D [de] MMMM [de] YYYY [a las] HH:MM'
                  )}
              </Typography>
            </>
          ) : (
            <Typography component="h5" variant="h5">
              Cerrada
            </Typography>
          )}
        </CardContent>
        {isAdmin && (
          <CardActions>
            {equity && equity.amount && (
              <Button
                onClick={() => this.handleClosePosition(market)}
                variant="contained"
                color="primary"
                size="small"
              >
                Cerrar Posición
              </Button>
            )}
            <Button
              onClick={() => this.handleOpenPosition(market)}
              variant="contained"
              color="secondary"
              size="small"
            >
              Abrir Posición
            </Button>
          </CardActions>
        )}
        {/* {market === 'DOW' && <CandleStickChart data={this.props.prices.charts.DOW || []} />}
        {market === 'DAX' && <CandleStickChart data={this.props.prices.charts.DAX || []} />} */}
        {/* <CandleStickChart data={prices.charts && prices.charts[market]} />} */}
      </Card>
    );
  }
}

export default withStyles(styles, { withTheme: true })(BalanceCard);

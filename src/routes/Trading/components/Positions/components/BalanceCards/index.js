import BalanceCardContainer from './container/BalanceCardContainer';

export { default as reducer } from '../../reducer/balance';

export default BalanceCardContainer;

import { connect } from 'react-redux';
import withStyles from '@mui/styles/withStyles';
import { actions as positionActions } from 'routes/Trading/components/Positions/reducer/positions';
import { actions as modalActions } from 'reducers/modal';
import { actions as balanceActions } from 'routes/Trading/components/Positions/reducer/balance';
import BalanceCards from '../components/BalanceCards';
import styles from '../styles/balanceCards';

const mapStateToProps = ({ trading }) => ({
  prices: trading.prices,
  equity: trading.balance.equity,
});

const mapDispatchToProps = {
  ...positionActions,
  ...modalActions,
  ...balanceActions,
};

const BalanceCardWrapped = withStyles(styles)(BalanceCards);

export default connect(mapStateToProps, mapDispatchToProps)(BalanceCardWrapped);

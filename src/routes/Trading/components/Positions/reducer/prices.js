import axios from 'config/axios';
import createReducer from 'redux/create-reducer';
import { COINBASE_PRICE, SET_SPREAD } from 'action-types';
import { getCryptoBalance } from './balance';

const getCoinbasePrice = (crypto) => async (dispatch) => {
  if (!crypto) {
    throw new Error('No viene crypto');
  }
  dispatch({ type: COINBASE_PRICE.REQUEST });

  try {
    const response = await axios(`trading/prices/coinbase/${crypto}`);

    dispatch({ type: COINBASE_PRICE.SUCCESS });
    dispatch({
      type: COINBASE_PRICE.SET,
      payload: response.data,
    });
    dispatch(getCryptoBalance(crypto, response.data));
  } catch (err) {
    dispatch({ type: COINBASE_PRICE.FAILURE });
    console.error(err);
  }
};

export const actions = {
  getCoinbasePrice,
};

const defaultState = {
  coinbase: {
    ETH: {},
  },
  spread: {},
};

const INITIAL_STATE = { ...defaultState };

const ACTION_HANDLERS = {
  [COINBASE_PRICE.SET]: (state, { payload }) => ({
    ...state,
    coinbase: {
      ...state.coinbase,
      ETH: payload,
    },
  }),
  [SET_SPREAD.SET]: (state, { payload }) => ({
    ...state,
    spread: {
      ...state.spread,
      ...payload,
    },
  }),
};

export default createReducer(INITIAL_STATE, ACTION_HANDLERS);

import styled from 'styled-components';
import { blue } from '@mui/material/colors';

const styles = (theme) => ({
  avatar: {
    backgroundColor: blue[100],
    color: blue[600],
  },
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  formControl: {
    margin: theme.spacing(1),
  },
  table: {
    root: {
      width: '100%',
      marginTop: theme.spacing(3),
      overflowX: 'auto',
    },
    table: {
      minWidth: 700,
    },
  },
  h1: {
    margin: '1rem 0',
    textTransform: 'uppercase',
    color: theme.palette.secondary.main,
    textShadow: `0.1rem 0.1rem ${theme.palette.primary.light}`,
    '@media (min-width: 960px)': {
      letterSpacing: '0.5rem',
    },
  },
  h2: {
    margin: '1rem 0',
    fontWeight: 100,
    color: theme.palette.secondary.main,
    textShadow: `0.05rem 0.05rem ${theme.palette.primary.light}`,
    '@media (max-width: 768px)': {
      margin: 0,
    },
  },
  fab: {
    position: 'absolute',
    bottom: theme.spacing(2),
    right: theme.spacing(2),
  },
});

export const PositionContainer = styled.div`
  margin: 1rem 0;

  @media (max-width: 960px) {
    display: none;
  }
`;

export const PositionsContainer = styled.div``;

export default styles;

import PositionsContainer from './container/PositionsContainer';

export { default as positionReducer } from './reducer/positions';
export { default as priceReducer } from './reducer/prices';

export default PositionsContainer;

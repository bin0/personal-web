import configureMockStore from 'redux-mock-store';
import axios from 'axios';
import thunk from 'redux-thunk';
// import fetchMock from "fetch-mock";
import positions from '../mocks/positions.json';
import { actions } from '../../reducer/positions';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

jest.mock('axios');

describe('positions module', () => {
  describe('getPositions()', () => {
    it('creates GET_POSITIONS_SET when fetching ETH positions has been done', async () => {
      const data = { data: [positions[0]] };
      axios.get = jest.fn().mockResolvedValue(data);

      const expectedActions = [
        { type: '/trading/GET_POSITIONS_REQUEST' },
        { type: '/trading/GET_POSITIONS_SUCCESS' },
        { type: '/trading/GET_POSITIONS_SET', payload: [positions[0]] },
      ];
      const store = mockStore({
        positions,
        trading: { prices: { ig: 10000 } },
      });

      return store.dispatch(actions.getPositions()).then(() => {
        expect(store.getActions()).toEqual(expectedActions);
      });
    });

    it('creates GET_POSITIONS_SET when fetching DOW positions has been done', async () => {
      const data = { data: [positions[1]] };
      axios.get = jest.fn().mockResolvedValue(data);

      const expectedActions = [
        { type: '/trading/GET_POSITIONS_REQUEST' },
        { type: '/trading/GET_POSITIONS_SUCCESS' },
        { type: '/trading/GET_POSITIONS_SET', payload: [positions[1]] },
      ];
      const store = mockStore({
        positions,
        trading: { prices: { ig: 10000 } },
      });

      return store.dispatch(actions.getPositions()).then(() => {
        expect(store.getActions()).toEqual(expectedActions);
      });
    });

    it('creates GET_POSITIONS_SET when fetching DAX positions has been done', async () => {
      const data = { data: [positions[2]] };
      axios.get = jest.fn().mockResolvedValue(data);

      const expectedActions = [
        { type: '/trading/GET_POSITIONS_REQUEST' },
        { type: '/trading/GET_POSITIONS_SUCCESS' },
        { type: '/trading/GET_POSITIONS_SET', payload: [positions[2]] },
      ];
      const store = mockStore({
        positions,
        trading: { prices: { ig: 10000 } },
      });

      return store.dispatch(actions.getPositions()).then(() => {
        expect(store.getActions()).toEqual(expectedActions);
      });
    });

    it('failws  when fetching positions', async () => {
      axios.get = jest.fn().mockRejectedValueOnce(new Error('error'));

      const expectedActions = [
        { type: '/trading/GET_POSITIONS_REQUEST' },
        { type: '/trading/GET_POSITIONS_FAILURE' },
      ];
      const store = mockStore({
        positions,
        trading: { prices: { ig: 10000 } },
      });

      return store.dispatch(actions.getPositions()).then(() => {
        expect(store.getActions()).toEqual(expectedActions);
      });
    });
  });

  describe('onOpenPosition(market, position)', () => {
    describe('when it runs', () => {
      it('should fetch data', async () => {
        const data = { data: positions };
        axios.get = jest.fn().mockResolvedValue(data);
        axios.post = jest.fn().mockResolvedValue();

        const expectedActions = [
          { type: '/trading/ADD_POSITION_REQUEST' },
          { type: '/trading/ADD_POSITION_SUCCESS' },
          { type: '/trading/GET_POSITIONS_REQUEST' },
        ];
        const store = mockStore({});

        return store.dispatch(actions.onOpenPosition('DOW', {})).then(() => {
          expect(store.getActions()).toEqual(expectedActions);
        });
      });
    });
    describe('when it fails', () => {
      it('should fetch data', async () => {
        axios.post = jest.fn().mockRejectedValueOnce(new Error('error'));

        const expectedActions = [
          { type: '/trading/ADD_POSITION_REQUEST' },
          { type: '/trading/ADD_POSITION_FAILURE' },
        ];
        const store = mockStore({});

        return store.dispatch(actions.onOpenPosition('DOW', {})).then(() => {
          expect(store.getActions()).toEqual(expectedActions);
        });
      });
    });
  });

  describe('onExitPosition(market, position)', () => {
    const store = mockStore({
      trading: {
        balance: { equity: { 'IX.D.DAX.IFS.IP': { quantity: 1 } } },
      },
    });
    describe('when it runs', () => {
      it('should fetch data', async () => {
        const data = { data: positions };
        axios.get = jest.fn().mockResolvedValue(data);
        axios.post = jest.fn().mockResolvedValue();

        const expectedActions = [
          { type: '/trading/EXIT_POSITION_REQUEST' },
          { type: '/trading/EXIT_POSITION_SUCCESS' },
          { type: '/trading/GET_POSITIONS_REQUEST' },
        ];

        return store
          .dispatch(actions.onExitPosition('DAX', { quantity: 2 }))
          .then(() => {
            expect(store.getActions()).toEqual(expectedActions);
          });
      });
    });
    describe('when it fails', () => {
      it('should fail while fetching data', async () => {
        axios.post = jest.fn().mockRejectedValueOnce(new Error('Error!!'));

        const expectedActions = 8;

        return store
          .dispatch(actions.onExitPosition('DAX', { quantity: 3 }))
          .then(() => {
            expect(store.getActions().length).toEqual(expectedActions);
          });
      });
    });
  });

  describe('deletePosition(market, position)', () => {
    const store = mockStore();

    describe('when it runs', () => {
      it('should fetch data', async () => {
        const data = { data: positions };
        axios.delete = jest.fn().mockResolvedValue(data);

        const expectedActions = 2;

        return store.dispatch(actions.deletePosition('DAX')).then(() => {
          expect(store.getActions().length).toEqual(expectedActions);
        });
      });
    });
    describe('when it fails', () => {
      it('should fail while fetching data', async () => {
        axios.delete = jest.fn().mockRejectedValueOnce(new Error('Error!!'));

        const expectedActions = 4;

        return store.dispatch(actions.deletePosition('DAX')).then(() => {
          expect(store.getActions().length).toEqual(expectedActions);
        });
      });
    });
  });
});

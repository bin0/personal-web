import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
// import fetchMock from "fetch-mock";
import positions from '../mocks/positions.json';
import {
  actions,
  updateBalance,
  getCryptoBalance,
} from '../../reducer/balance';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

jest.mock('axios');

describe.only('trades balances module', () => {
  describe('getIndexBalance()', () => {
    describe('when long', () => {
      it('should get current balance', async () => {
        const expectedData = {
          'IX.D.DAX.IFS.IP': {
            mediumPrice: 12202.95,
            quantity: 4,
            amount: -367.8,
            startTrade: '2018-05-07T08:25:33.183Z',
            direction: 'Long',
          },
        };
        const expectedActions = [
          { type: '/trading/GET_INDEX_BALANCE_SET', payload: expectedData },
        ];
        const store = mockStore({
          positions,
          trading: {
            positions: { open: positions },
            prices: { ig: { 'IX.D.DAX.IFS.IP': 12220 } },
          },
        });

        return store
          .dispatch(
            actions.getIndexBalance('IX.D.DAX.IFS.IP', {
              'IX.D.DAX.IFS.IP': { OFFER: 12222, BID: 12111 },
            })
          )
          .then(() => {
            expect(store.getActions()).toEqual(expectedActions);
          });
      });
      it('if no positions open should return []', async () => {
        const expectedData = {
          'IX.D.DAX.IFS.IP': {},
        };
        const expectedActions = [
          { type: '/trading/GET_INDEX_BALANCE_SET', payload: expectedData },
        ];
        const store = mockStore({
          positions,
          trading: {
            positions: { open: [] },
            prices: { ig: { 'IX.D.DAX.IFS.IP': 12220 } },
          },
        });

        return store
          .dispatch(
            actions.getIndexBalance('IX.D.DAX.IFS.IP', {
              'IX.D.DAX.IFS.IP': { OFFER: 12222, BID: 12111 },
            })
          )
          .then(() => {
            expect(store.getActions()).toEqual(expectedActions);
          });
      });
    });
    describe('when short', () => {
      it('should get current balance', async () => {
        const expectedData = {
          'IX.D.DOW.IFS.IP': {
            mediumPrice: 21202.95,
            quantity: 2,
            amount: 17961.9,
            startTrade: '2018-05-07T08:25:33.183Z',
            direction: 'Short',
          },
        };
        const expectedActions = [
          { type: '/trading/GET_INDEX_BALANCE_SET', payload: expectedData },
        ];
        const store = mockStore({
          positions,
          trading: {
            positions: { open: positions },
            prices: { ig: { 'IX.D.DOW.IFS.IP': 12220 } },
          },
        });

        return store
          .dispatch(
            actions.getIndexBalance('IX.D.DOW.IFS.IP', {
              'IX.D.DOW.IFS.IP': { OFFER: 12222, BID: 12111 },
            })
          )
          .then(() => {
            expect(store.getActions()).toEqual(expectedActions);
          });
      });
    });

    // it("failws  when fetching positions", async () => {
    //   axios.get = jest.fn().mockRejectedValueOnce(new Error("error"));

    //   const expectedActions = [
    //     { type: "/trading/GET_POSITIONS_REQUEST" },
    //     { type: "/trading/GET_POSITIONS_FAILURE" },
    //   ];
    //   const store = mockStore({
    //     positions,
    //     trading: { prices: { ig: 10000 } },
    //   });

    //   return store.dispatch(actions.getPositions()).then(() => {
    //     expect(store.getActions()).toEqual(expectedActions);
    //   });
    // });
  });

  describe('updateBalance()', () => {
    describe('when it runs', () => {
      it('should get current balance', async () => {
        const expectedData = { DAX: 200 };
        const expectedActions = [
          { type: '/trading/GET_INDEX_BALANCE_SET', payload: expectedData },
        ];
        const store = mockStore({
          positions,
          trading: {
            positions: { selectedMarket: 'DOW', open: positions },
            balance: { equity: { DOW: 100, DAX: 200 } },
            prices: { ig: { 'IX.D.DAX.IFS.IP': 12220 } },
          },
        });

        store.dispatch(updateBalance());
        expect(store.getActions()).toEqual(expectedActions);
      });
    });
  });

  describe('getCryptoBalance()', () => {
    describe('when it runs', () => {
      it('should get current balance', async () => {
        const expectedData = {
          'ETH-EUR': {
            amount: -382.74907499999995,
            direction: 'Long',
            mediumPrice: 202.95,
            quantity: 7.2285,
            startTrade: '2018-05-07T08:25:33.183Z',
          },
        };
        const expectedActions = [
          { type: '/trading/GET_CRYPTO_BALANCE_SET', payload: expectedData },
        ];
        const store = mockStore({
          positions,
          trading: {
            positions: { selectedMarket: 'DOW', open: positions },
            balance: { equity: { 'ETH-EUR': 100 } },
            prices: { coinbase: { 'ETH-EUR': 100 } },
          },
        });

        store.dispatch(getCryptoBalance('ETH-EUR', { amount: 150 }));
        expect(store.getActions()).toEqual(expectedActions);
      });
    });
  });
});

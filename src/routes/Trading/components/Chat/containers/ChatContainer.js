import { connect } from 'react-redux';

import withStyles from '@mui/styles/withStyles';

import ChatView from '../components/ChatView';
import styles from '../styles/chat';
import { actions as chatActions } from 'reducers/chat';

// FIXME checkout https://mui.com/components/use-media-query/#migrating-from-withwidth
const withWidth = () => (WrappedComponent) => (props) =>
  <WrappedComponent {...props} width="xs" />;

const mapStateToProps = ({ user, chat }) => ({
  messages: chat.messages,
  chatActive: chat.chatActive,
  user,
});

const mapDispatchToProps = { ...chatActions };

const StyledChat = withStyles(styles)(withWidth()(ChatView));

export default connect(mapStateToProps, mapDispatchToProps)(StyledChat);

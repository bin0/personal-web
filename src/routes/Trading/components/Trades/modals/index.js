import TradesMoreInfo from './TradesMoreInfo';

const modals = {
  TRADES_MORE_INFO: TradesMoreInfo,
};

export default modals;

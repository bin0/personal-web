import React from 'react';
import PropTypes from 'prop-types';
import withStyles from '@mui/styles/withStyles';
import Typography from '@mui/material/Typography';
import DialogTitle from '@mui/material/DialogTitle';
import DialogContent from '@mui/material/DialogContent';
import Dialog from '@mui/material/Dialog';
import Button from '@mui/material/Button';
import { DialogActions } from '@mui/material';
import styles from '../styles/trades';

const TradesMoreInfo = ({ close, open }) => {
  const handleCloseModal = () => {
    close();
  };

  return (
    <Dialog onClose={close} aria-labelledby="exit-position-dialog" open={open}>
      <DialogTitle id="exit-position-dialog">Balance Cuenta Real</DialogTitle>
      <DialogContent>
        <Typography>
          Este es el resultado obtenido en € durante el número de operaciones
          mostrado
        </Typography>
      </DialogContent>
      <DialogActions>
        <Button onClick={handleCloseModal} variant="contained" color="primary">
          Entendido
        </Button>
      </DialogActions>
    </Dialog>
  );
};

TradesMoreInfo.propTypes = {
  classes: PropTypes.object.isRequired,
  close: PropTypes.func.isRequired,
  open: PropTypes.bool.isRequired,
};

const TradesMoreInfoWrapped = withStyles(styles)(TradesMoreInfo);

export default TradesMoreInfoWrapped;

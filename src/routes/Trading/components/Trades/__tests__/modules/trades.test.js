import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import { actions } from '../../modules/trades';
import axios from 'axios';
import { DOW, DAX } from '../mocks/markets.json';
import equity from '../mocks/equity.json';
import trades from '../mocks/trades.json';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);
jest.mock('axios');

describe('trades module', () => {
  it('creates CALCULATE_EQUITY_SET when fetching trades has been done', () => {
    axios.get = jest.fn().mockResolvedValue({ data: trades });

    const expectedActions = [
      { type: '/trading/GET_TRADES_REQUEST' },
      { type: '/trading/GET_TRADES_SUCCESS' },
      { type: '/trading/GET_TRADES_SET', payload: { DOW, DAX } },
      { type: '/trading/CALCULATE_EQUITY_SET', payload: equity },
    ];
    const store = mockStore({ market: { DOW, DAX }, equity });

    return store.dispatch(actions.getTrades()).then(() => {
      // return of async actions
      expect(store.getActions()).toEqual(expectedActions);
    });
  });
});

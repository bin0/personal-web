import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Button } from '@mui/material';
import Analitycs from 'utils/analitycs/google-analitycs';
import Separator from 'components/Separator';
import { TradesContainer } from '../styles/trades';
import Balance from './BalanceView';
import { currentMonthText, currentYear } from 'utils/dates';

class Trades extends Component {
  componentDidMount() {
    this.getTrades();
  }

  handleMoreInfoClick = () => {
    this.props.openModal({ modalType: 'TRADES_MORE_INFO', modalProps: {} });
    Analitycs.event({
      label: 'Trades',
      category: 'Trading',
      action: 'More Info',
      nonInteraction: false,
    });
  };

  getTrades = () => {
    this.props.getTrades();
  };

  render() {
    const { classes, equity, tournament } = this.props;

    return (
      <TradesContainer>
        <h1 className={classes.h1}>
          Trades finalizados {currentMonthText()} {currentYear}
        </h1>
        <Balance
          title="Número de pips"
          classes={classes}
          equity={tournament || equity}
        />
        <Separator />
        <Button
          onClick={this.handleMoreInfoClick}
          variant="contained"
          color="secondary"
          size="small"
        >
          Más información
        </Button>
      </TradesContainer>
    );
  }
}

Trades.propTypes = {
  classes: PropTypes.object.isRequired,
  equity: PropTypes.array.isRequired,
  tournament: PropTypes.array.isRequired,
  openModal: PropTypes.func.isRequired,
};

export default Trades;

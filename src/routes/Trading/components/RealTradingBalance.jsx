import React, { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { actions } from '../modules/trades';

const RealTradingBalance = ({ realTrades }) => {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(actions.getRealTradesHistory());
  }, [dispatch]);

  return realTrades.map((trade) => (
    <div key={trade.id}>
      Fecha: {trade.startDate}
      Cantidad: {trade.amount}
    </div>
  ));
};

export default RealTradingBalance;

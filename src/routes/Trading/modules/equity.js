import createReducer from 'redux/create-reducer';
import { SET_ACCOUNT_EQUITY } from 'action-types';

// const setAccountSubscription = (data) => (dispatch) => {
//   const list = {};
//   const mapFields = (item) => {
//     list[item] = data.getValue(item);
//   };

//   data.forEachChangedField(mapFields);
//   console.log('list', list);
//   dispatch({ type: SET_ACCOUNT_EQUITY.SET, payload: list });
// };

export const actions = {};

export const INITIAL_STATE = {
  data: {},
  dataLoaded: false,
};

export const ACTION_HANDLERS = {
  [SET_ACCOUNT_EQUITY.SET]: (state, { payload }) => ({
    ...state,
    data: payload,
    dataLoaded: true,
  }),
};

export default createReducer(INITIAL_STATE, ACTION_HANDLERS);

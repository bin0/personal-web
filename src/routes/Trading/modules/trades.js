import createReducer from 'redux/create-reducer';
import axios from 'config/axios';
import { GET_REAL_TRADES_HISTORY } from 'action-types';

// const tradesHistory = [
//   {
//     id: 1,
//     amount: 10,
//     startDate: new Date(),
//   },
// ];

const getRealTradesHistory = () => async (dispatch) => {
  dispatch({ type: GET_REAL_TRADES_HISTORY.REQUEST });

  try {
    const URL = '/trading/ig/trades';
    const response = await axios.get(URL);

    dispatch({ type: GET_REAL_TRADES_HISTORY.SUCCESS });
    dispatch({ type: GET_REAL_TRADES_HISTORY.SET, payload: response.data });
  } catch (error) {
    dispatch({ type: GET_REAL_TRADES_HISTORY.FAILURE });
  }
};
export const actions = {
  getRealTradesHistory,
};

export const INITIAL_STATE = {
  list: [],
};

export const ACTION_HANDLERS = {
  [GET_REAL_TRADES_HISTORY.SET]: (state, { payload }) => ({
    ...state,
    realTrades: payload,
  }),
};

export default createReducer(INITIAL_STATE, ACTION_HANDLERS);

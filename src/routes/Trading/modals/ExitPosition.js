import React from 'react';
import PropTypes from 'prop-types';
import withStyles from '@mui/styles/withStyles';

import DialogTitle from '@mui/material/DialogTitle';
import DialogContent from '@mui/material/DialogContent';
import Dialog from '@mui/material/Dialog';
import Input from '@mui/material/Input';
import Button from '@mui/material/Button';
import FormControl from '@mui/material/FormControl';
import InputLabel from '@mui/material/InputLabel';
import { DialogActions } from '@mui/material';

import styles from '../styles/trading';
import { MARKETS } from '../modules/constants';

class ExitPosition extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      exitPrice: props.ig[MARKETS.IG[props.selectedMarket]].BID,
      quantity: 1,
      direction: 'Long',
    };
  }

  componentDidMount() {
    const positions = this.props.positions.filter(
      (pos) => pos.market === this.props.selectedMarket
    );

    const quantity = positions.reduce((total, pos) => total + pos.quantity, 0);
    if (positions[0].direction !== 'Long') {
      this.setState({
        exitPrice: this.props.ig[MARKETS.IG[this.props.selectedMarket]].OFFER,
      });
    }

    this.setState({ direction: positions[0].direction, quantity });
  }

  handleChange = ({ target: { name, value } }) => {
    this.setState({ [name]: parseFloat(value) });
  };

  handleOnExitPosition = () => {
    this.props.onExitPosition(this.props.selectedMarket, this.state);
    this.handleClose();
  };

  handleClose = () => {
    this.props.closeModal();
  };

  render() {
    const { classes, closeModal, open } = this.props;

    return (
      <Dialog
        onClose={closeModal}
        aria-labelledby="exit-position-dialog"
        open={open}
      >
        <DialogTitle id="exit-position-dialog">Cerrando posición</DialogTitle>
        <DialogContent>
          <FormControl className={classes.formControl} variant="filled">
            <InputLabel htmlFor="exitPrice">Precio Salida</InputLabel>
            <Input
              type="number"
              id="exitPrice"
              name="exitPrice"
              value={this.state.exitPrice}
              onChange={this.handleChange}
            />
          </FormControl>
          <FormControl className={classes.formControl} variant="filled">
            <InputLabel htmlFor="quantity">Contratos</InputLabel>
            <Input
              type="number"
              id="quantity"
              name="quantity"
              value={this.state.quantity}
              onChange={this.handleChange}
            />
          </FormControl>
        </DialogContent>
        <DialogActions>
          <Button
            onClick={this.handleOnExitPosition}
            variant="contained"
            color="primary"
          >
            cerrar posición
          </Button>
        </DialogActions>
      </Dialog>
    );
  }
}

ExitPosition.propTypes = {
  classes: PropTypes.object.isRequired,
  closeModal: PropTypes.func.isRequired,
  onExitPosition: PropTypes.func.isRequired,
  selectedMarket: PropTypes.string.isRequired,
};

const ExitPositionWrapped = withStyles(styles)(ExitPosition);

export default ExitPositionWrapped;

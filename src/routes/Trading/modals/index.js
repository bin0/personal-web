import SelectMarket from './SelectMarket';
import NewTrade from './NewTrade';
import ExitPosition from './ExitPosition';
import Trades from '../components/Trades/modals';

const modals = {
  SELECT_MARKET: SelectMarket,
  NEW_TRADE: NewTrade,
  EXIT_POSITION: ExitPosition,
  ...Trades,
};

export default modals;

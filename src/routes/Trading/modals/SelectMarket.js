import React from 'react';
import PropTypes from 'prop-types';
import withStyles from '@mui/styles/withStyles';
import Avatar from '@mui/material/Avatar';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemAvatar from '@mui/material/ListItemAvatar';
import ListItemText from '@mui/material/ListItemText';
import DialogTitle from '@mui/material/DialogTitle';
import Dialog from '@mui/material/Dialog';
import PersonIcon from '@mui/icons-material/Person';
import { blue } from '@mui/material/colors';

const products = ['DAX', 'DOW', 'ETH'];
const styles = {
  avatar: {
    backgroundColor: blue[100],
    color: blue[600],
  },
};

class SelectMarket extends React.Component {
  handleSelectMarketClick = (value) => {
    this.handleClose();
    this.props.onSelectMarket(value);
    this.props.openModal('NEW_TRADE');
  };

  handleClose = () => {
    this.props.closeModal();
  };

  render() {
    const { classes, closeModal, open } = this.props;

    return (
      <Dialog
        onClose={closeModal}
        aria-labelledby="select-market-dialog"
        open={open}
      >
        <DialogTitle id="select-market-dialog">Selecciona activo</DialogTitle>
        <div>
          <List>
            {products.map((product) => (
              <ListItem
                button
                onClick={() => this.handleSelectMarketClick(product)}
                key={product}
              >
                <ListItemAvatar>
                  <Avatar className={classes.avatar}>
                    <PersonIcon />
                  </Avatar>
                </ListItemAvatar>
                <ListItemText primary={product} />
              </ListItem>
            ))}
          </List>
        </div>
      </Dialog>
    );
  }
}

SelectMarket.propTypes = {
  classes: PropTypes.object.isRequired,
  closeModal: PropTypes.func.isRequired,
  openModal: PropTypes.func.isRequired,
  onSelectMarket: PropTypes.func.isRequired,
};

const SelectMarketWrapped = withStyles(styles)(SelectMarket);

export default SelectMarketWrapped;

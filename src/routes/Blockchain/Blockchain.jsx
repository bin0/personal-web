import styled from 'styled-components';
import React from 'react';
import { Typography } from '@mui/material';

const BlockChain = () => {
  return (
    <StyledContainer>
      <img src="/logos/Ethereum_logo.svg" alt="ethereum" width="100" />
      <Typography variant="h2">
        Máster en BlockChain, SmartContracts y Criptoeconomía
      </Typography>
      <Typography variant="h4">Web3 Developer</Typography>
    </StyledContainer>
  );
};

const StyledContainer = styled.div`
  min-height: calc(100vh - 64px - 124px);
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  img {
    margin: 20px auto;
    color: red;
    fill: red;
    stroke: red;
    background-color: white;
  }
`;

export default BlockChain;

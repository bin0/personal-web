import React, { lazy } from 'react';
import Home from 'components/Home';
import RouteWithLayout from './RouteWithLayout';
import BlockChain from './Blockchain';

const DeveloperLayout = lazy(() => import('layouts/DeveloperLayout'));
const MainLayout = lazy(() => import('layouts/MainLayout'));
const PortfolioLayout = lazy(() => import('layouts/PortfolioLayout'));
const TradingLayout = lazy(() => import('layouts/TradingLayout'));
const Biography = lazy(() => import('routes/Biography'));
const Developer = lazy(() => import('routes/Developer'));
const Portfolio = lazy(() => import('routes/Portfolio'));
const Trading = lazy(() => import('routes/Trading'));
const Web3 = lazy(() => import('routes/Web3'));

const routesBoot = [
  <RouteWithLayout
    key="/"
    path="/"
    exact
    component={Home}
    layout={MainLayout}
  />,
  <RouteWithLayout
    key="trading"
    path="/trading*"
    component={Trading}
    layout={TradingLayout}
  />,
  <RouteWithLayout
    key="blockchain"
    path="/blockchain"
    component={BlockChain}
    layout={TradingLayout}
  />,
  <RouteWithLayout
    key="portfolio"
    path="/portfolio"
    exact
    component={Portfolio}
    layout={PortfolioLayout}
  />,
  <RouteWithLayout
    key="developer"
    path="/developer"
    component={Developer}
    layout={DeveloperLayout}
  />,
  <RouteWithLayout
    key="tools"
    path="/tools"
    exact
    component={Home}
    layout={MainLayout}
  />,
  <RouteWithLayout
    key="biography"
    path="/biography"
    exact
    component={Biography}
    layout={MainLayout}
  />,
  <RouteWithLayout
    key="web3"
    path="/web3"
    exact
    component={Web3}
    layout={MainLayout}
  />,
];

export default routesBoot;

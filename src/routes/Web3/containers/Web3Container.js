import { connect } from 'react-redux';
import { hideLoading } from 'react-redux-loading-bar';
import { actions as etherActions } from 'reducers/ethereum/ethers';
import { actions as tokenActions } from 'reducers/ethereum/tokens';
import { actions as walletActions } from 'reducers/ethereum/wallet';
import { actions as networkActions } from 'reducers/ethereum/network';
import Web3View from '../components/Web3View';
import '../styles/web3.scss';

export const mapStateToProps = ({ ethereum }) => ({ ...ethereum.ethers });

export const mapDispatchToProps = {
  hideLoading,
  ...etherActions,
  ...tokenActions,
  getTokenBalance: () => (dispatch) =>
    dispatch(
      tokenActions.getTokenBalance(['WBTC', 'OVR', 'DAI', 'LINK', 'GRT'])
    ),
  ...walletActions,
  ...networkActions,
};

export default connect(mapStateToProps, mapDispatchToProps)(Web3View);

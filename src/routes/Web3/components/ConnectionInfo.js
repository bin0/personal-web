import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Button, Typography, Box, Stack } from '@mui/material';
import { actions } from 'reducers/ethereum/network';
import Grow from '@mui/material/Grow';
import { getChainName } from 'types';

const ConnectionInfo = () => {
  const dispatch = useDispatch();
  const { chainId } = useSelector(({ ethereum }) => ethereum.network);

  /**********************************************************/
  /* Handle chain (network) and chainChanged (per EIP-1193) */
  /**********************************************************/

  const handleChangeNetwork = (networkId) => () => {
    dispatch(actions.changeNetwork(networkId));
  };

  return (
    <Box mt={2}>
      <Typography gutterBottom variant="h2">
        Network Info
      </Typography>
      <Grow appear={true} in unmountOnExit>
        <Box mb={2}>
          <Stack direction="row" spacing={1}>
            <Typography>Connected Chain:</Typography>
            <Typography color="secondary">
              {!!chainId ? getChainName(chainId) : '---'}
            </Typography>
          </Stack>
        </Box>
      </Grow>
      <Stack direction="row" spacing={2}>
        {chainId !== 137 && (
          <Button
            variant="contained"
            color="secondary"
            onClick={handleChangeNetwork(137)}
          >
            Polygon Network
          </Button>
        )}
        {chainId !== 1 && (
          <Button
            variant="contained"
            color="secondary"
            onClick={handleChangeNetwork(1)}
          >
            Ethereum Mainnet Network
          </Button>
        )}
        {chainId !== 1088 && (
          <Button
            variant="contained"
            color="secondary"
            onClick={handleChangeNetwork(1088)}
          >
            Metis Network
          </Button>
        )}
      </Stack>
    </Box>
  );
};

export default React.memo(ConnectionInfo);

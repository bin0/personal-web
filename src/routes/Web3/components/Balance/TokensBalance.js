import React from 'react';
import { Box, Grid, Grow, Typography } from '@mui/material';
import { useSelector } from 'react-redux';
import { formatCurrency } from 'utils/strings';

const TokensBalance = () => {
  const { tokens } = useSelector((state) => state.ethereum);

  return Object.entries(tokens).map(
    ([key, value], index) =>
      value && (
        <Grid width={240} item xs={12} sm={6} md={4} lg={3} key={key}>
          <Grow timeout={500 * (index + 1)} in unmountOnExit>
            <Box
              p={2}
              bgcolor="secondary.main"
              height={100}
              sx={{ borderRadius: '5px' }}
            >
              <Typography variant="h4" color="primary">
                {key}: {formatCurrency('es-ES', value.balance, value.decimals)}
              </Typography>
            </Box>
          </Grow>
        </Grid>
      )
  );
};

export default React.memo(TokensBalance);

import React from 'react';
import { useSelector } from 'react-redux';
import { Box, Grid, Typography, Grow, Stack } from '@mui/material';
import { hideAddress } from 'utils';
import TokensBalance from './TokensBalance';

const Balance = () => {
  const { wallet } = useSelector((state) => state.ethereum);

  return (
    <Box mt={2}>
      <Typography variant="h2" gutterBottom>
        Wallet Balance
      </Typography>
      <Grow appear={true} in unmountOnExit>
        <Box mb={2}>
          <Stack direction="row" spacing={1}>
            <Typography>Connected Account:</Typography>
            <Typography color="secondary">
              {hideAddress(wallet.walletAddress)}
            </Typography>
          </Stack>
        </Box>
      </Grow>
      <Typography paragraph></Typography>
      <Box mt={2}>
        <Grid container spacing={2}>
          <TokensBalance />
        </Grid>
      </Box>
    </Box>
  );
};

export default React.memo(Balance);

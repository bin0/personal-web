import React from 'react';
import { Button, Box } from '@mui/material';
import Grow from '@mui/material/Grow';

const SendFunds = () => {
  return (
    <Box my={2}>
      <Grow style={{ transform: '0 0 0' }} in unmountOnExit>
        <Button variant="contained" color="secondary">
          Enviar fondos
        </Button>
      </Grow>
    </Box>
  );
};

export default React.memo(SendFunds);

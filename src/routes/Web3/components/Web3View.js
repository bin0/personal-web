import React, { useEffect, useMemo } from 'react';
import { useSelector } from 'react-redux';
import { Box, CircularProgress, Stack } from '@mui/material';
import { chains } from 'types';
import { findById } from 'utils/arrays';
import { getChainIdSelector } from 'reducers/ethereum/network';
import { getEthersErrorSelector } from 'reducers/ethereum/ethers';
import Balance from './Balance';
import ConnectionInfo from './ConnectionInfo';
// import SendFunds from './SendFunds';
import SwapView from './Swap';
import ConnectionError from './ConnectionError';
import UnsupportedNetwork from './UnsupportedNetwork';

const Web3View = ({
  initEthers,
  getWalletAddress,
  getEthBalance,
  getTokenBalance,
  getNetwork,
  hideLoading,
}) => {
  const chainId = useSelector(getChainIdSelector);
  const error = useSelector(getEthersErrorSelector);
  const isLoading = useSelector((state) => !!state.loadingBar.default);

  useEffect(initEthers, [initEthers]);
  useEffect(getNetwork, [getNetwork]);
  useEffect(getWalletAddress, [getWalletAddress]);
  useEffect(getTokenBalance, [getTokenBalance]);
  useEffect(getEthBalance, [getEthBalance]);
  useEffect(hideLoading, [hideLoading]);

  const isValid = useMemo(() => chains.some(findById(chainId)), [chainId]);

  if (isLoading) {
    return (
      <Box mt={5} mx={3} display="flex" justifyContent="center">
        <CircularProgress color="secondary" size={120} />
      </Box>
    );
  }

  return (
    <Box mx={2}>
      {error && <ConnectionError />}
      {!error && !isValid && <UnsupportedNetwork />}
      {!error && isValid && (
        <Stack spacing={2}>
          <ConnectionInfo />
          <Balance />
          {/* <SendFunds /> */}
          <SwapView />
        </Stack>
      )}
    </Box>
  );
};

export default Web3View;

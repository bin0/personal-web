import { connect } from 'react-redux';
import loadingBarActions from 'react-redux-loading-bar';
import { actions as tokenActions } from 'reducers/ethereum/tokens';
import { actions as walletActions } from 'reducers/ethereum/wallet';
import { actions as networkActions } from 'reducers/ethereum/network';
import SwapView from '../components/SwapView';

export const mapStateToProps = ({ ethereum }) => ({ ...ethereum.ethers });

export const mapDispatchToProps = {
  ...loadingBarActions,
  ...tokenActions,
  ...walletActions,
  ...networkActions,
};

export default connect(mapStateToProps, mapDispatchToProps)(SwapView);

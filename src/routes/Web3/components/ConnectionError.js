import BrowserNotSupportedIcon from '@mui/icons-material/BrowserNotSupported';
import { Box, Button, Stack, Typography } from '@mui/material';

const ConnectionError = () => {
  return (
    <Box p={2} bgcolor="secondary.main" sx={{ borderRadius: 3 }} maxWidth={360}>
      <Stack spacing={2} alignItems="center">
        <BrowserNotSupportedIcon color="error" style={{ fontSize: 100 }} />
        <Typography color="error" variant="h2">
          Wallet Not Connected
        </Typography>
        <Typography color="primary" variant="subtitle1" textAlign="justify">
          Parece que tu wallet no está conectado, puedes empezar por{' '}
          <b>conectar con MetaMask</b>, por ejemplo.
        </Typography>
        <Button variant="contained" color="secondary">
          <Typography variant="body1">Conectar con MetaMask</Typography>
        </Button>
      </Stack>
    </Box>
  );
};

export default ConnectionError;

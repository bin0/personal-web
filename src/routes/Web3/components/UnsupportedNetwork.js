import BrowserNotSupportedIcon from '@mui/icons-material/BrowserNotSupported';
import { Box, Stack, Typography } from '@mui/material';

const UnsupportedNetwork = () => {
  return (
    <Box p={2} bgcolor="secondary.main" sx={{ borderRadius: 3 }} maxWidth={360}>
      <Stack spacing={2} alignItems="center">
        <BrowserNotSupportedIcon color="error" style={{ fontSize: 100 }} />
        <Typography color="error" variant="h2">
          Network Not Supported
        </Typography>
        <Typography color="primary" variant="subtitle1" textAlign="justify">
          Parece que tu navegador <b>no soporta Web3</b>, o estás en una ventana
          de incógnito, prueba de otra forma para acceder a las funcionalidades.
        </Typography>
      </Stack>
    </Box>
  );
};

export default UnsupportedNetwork;

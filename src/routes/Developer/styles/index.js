import styled from 'styled-components';
import { Link } from 'react-router-dom';
import Card from '@mui/material/Card';
import DobleArrowIcon from '@mui/icons-material/DoubleArrow';

export const DeveloperViewContainerStyled = styled.div`
  margin: 1rem;
  a {
    color: inherit;
    text-decoration: none;

    &:hover {
      text-decoration: underline;
    }
  }
`;

export const DetailsListBoxContainerStyled = styled.div`
  margin: 0;
  padding-top: 0;
`;

export const ListContainerStyled = styled.div`
  margin-top: 1rem;
`;

export const LinkStyled = styled(Link)`
  color: #8ee7ff;
  text-decoration: none;

  a&:hover {
    text-decoration: underline;
    text-decoration-color: #8ee7ff;
  }
`;

export const FrontendContainerStyled = styled.section`
  margin: 1rem;

  b {
    color: #8ee7ff;
  }
  ul {
    list-style-type: none;
    margin: 0;
  }
`;

export const FrontendHeaderStyled = styled.header`
  margin: 1rem auto;
  display: flex;
  align-items: center;

  h1 {
    margin-left: 1rem;
  }
`;

export const BackendContainerStyled = styled.section`
  margin: 1rem;

  ul {
    list-style-type: none;
    margin: 0;
  }
`;

export const CardContainerStyled = styled(Card)`
  margin-bottom: 0.5rem;
`;

export const DobleArrowIconStyled = styled(DobleArrowIcon)`
  cursor: pointer;
  position: relative;

  &:hover {
    animation: card-link infinite 0.4s;
  }

  @keyframes card-link {
    from {
      opacity: 1;
      left: -3px;
    }
    to {
      opacity: 0.5;
      left: 3px;
    }
  }
`;

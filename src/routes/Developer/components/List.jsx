import React from 'react';
import { Typography } from '@mui/material';
import { ListContainerStyled, LinkStyled } from '../styles';

const List = ({ title, children }) => (
  <ListContainerStyled>
    <LinkStyled to={`/developer/${title.toLowerCase()}`}>
      <Typography color="secondary" variant="h2">
        {title}
      </Typography>
    </LinkStyled>
    <ul>{children}</ul>
  </ListContainerStyled>
);

export default List;

import React from 'react';
import styled from 'styled-components';
import { Typography } from '@mui/material';
import Navigation from 'components/Navigation';

const SocialContainerStyled = styled.section`
  margin: 1rem;
`;

const Social = () => {
  return (
    <SocialContainerStyled>
      <Navigation back />
      <Typography variant="h1">Social</Typography>
    </SocialContainerStyled>
  );
};

export default Social;

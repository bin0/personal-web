import React from 'react';
import { Typography, ListItemText, List } from '@mui/material';

const Competencies = () => (
  <div>
    <Typography variant="h2" color="secondary">
      Competency Profile
    </Typography>
    <List>
      <ListItemText primary="Exceptional communication skills." />
      <ListItemText primary="+10 years of experience in commercial software development." />
      <ListItemText primary="Expert knowledge of JavaScript, React Ecosystem." />
      <ListItemText primary="Expert knowledge of web technologies (HTML/CSS)." />
      <ListItemText primary="High level of self-organization, ownership, responsibility." />
      <ListItemText primary="GraphQL, WebSockets, Agile, SCRUM, JIRA, Git Flow" />
      <ListItemText primary="Experience working with REST Web APIs and JSON." />
      <ListItemText primary="Min. upper-intermediate English level." />
    </List>
  </div>
);

export default Competencies;

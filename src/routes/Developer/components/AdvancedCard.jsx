import React, { useMemo, useCallback } from 'react';
import PropTypes from 'prop-types';
import { useHistory, Link } from 'react-router-dom';
import {
  Card,
  CardHeader,
  CardContent,
  ListItemText,
  CardActions,
  Button,
} from '@mui/material';
import CardAvatar from './CardAvatar';
import { DobleArrowIconStyled } from '../styles';
import { capitalize } from 'utils/strings';
import { reduceListByFields } from 'utils/arrays';
import * as sections from '../constants';

const byTitleAndList = reduceListByFields('title', 'list');

const AdvancedCard = ({ section, subheader }) => {
  const history = useHistory();

  const handleClick = useCallback(() => {
    history.push(`/developer/${section}`);
  }, [history, section]);

  const cardsList = useMemo(
    () => sections[section].reduce(byTitleAndList, []),
    [section]
  );

  return (
    <Card>
      <CardHeader
        avatar={<CardAvatar item={section} />}
        action={<DobleArrowIconStyled onClick={handleClick} />}
        title={<Link to={`/developer/${section}`}>{capitalize(section)}</Link>}
        subheader={subheader}
      />
      <CardContent>
        {cardsList.slice(0, 3).map((item) => (
          <ListItemText secondary={item} key={item} />
        ))}
        <ListItemText secondary="..." />
      </CardContent>
      <CardActions>
        <Button color="primary" onClick={handleClick} variant="contained">
          ver más
        </Button>
      </CardActions>
    </Card>
  );
};

AdvancedCard.defaultProps = {
  subheader: '',
};

AdvancedCard.propTypes = {
  section: PropTypes.string.isRequired,
  subheader: PropTypes.string.isRequired,
};

export default AdvancedCard;

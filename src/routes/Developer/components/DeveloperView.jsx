import React from 'react';
import { Helmet } from 'react-helmet';
import Grid from '@mui/material/Grid';
import Typography from '@mui/material/Typography';
import Navigation from 'components/Navigation';
import Competencies from './Competencies';
import Breafing from './Breafing';
import AdvancedCard from './AdvancedCard';
import { DeveloperViewContainerStyled } from '../styles';

const DeveloperView = () => (
  <DeveloperViewContainerStyled>
    <Helmet>
      <title>Senior FullStack Developer</title>
      <meta
        name="description"
        content="Professional Developer, a Senior FullStack Developer with passion to the things he does."
      />
    </Helmet>
    <Navigation back />
    <Typography variant="h1">Developer Profile</Typography>
    <Breafing />
    <Competencies />
    <Grid container spacing={1}>
      <Grid item xs={12} sm={6} md={4} xl={3}>
        <AdvancedCard section="frontend" subheader="Web & mobile" />
      </Grid>
      <Grid item xs={12} sm={6} md={4} xl={3}>
        <AdvancedCard section="backend" subheader="RestFull JSON" />
      </Grid>
      <Grid item xs={12} sm={6} md={4} xl={3}>
        <AdvancedCard section="devops" subheader="Deploys CI" />
      </Grid>
      <Grid item xs={12} sm={6} md={4} xl={3}>
        <AdvancedCard section="tools" subheader="Working with" />
      </Grid>
      <Grid item xs={12} sm={6} md={4} xl={3}>
        <AdvancedCard section="repos" subheader="Version control" />
      </Grid>
      <Grid item xs={12} sm={6} md={4} xl={3}>
        <AdvancedCard section="social" subheader="Q&A Help" />
      </Grid>
    </Grid>
  </DeveloperViewContainerStyled>
);

export default DeveloperView;

import React from 'react';
import { Typography } from '@mui/material';

const Breafing = () => (
  <div>
    <Typography color="secondary" variant="h2" paragraph>
      Breafing
    </Typography>
    <Typography paragraph variant="body2">
      <i>"Enthusiast, code friendly developer"</i>
    </Typography>
    <Typography paragraph>
      "Kaizen" is a must, usually envolved in never ending personal projects
      while looking for perfection.
    </Typography>
    <Typography paragraph>
      Programming from MacOS to almost all platforms.
    </Typography>
  </div>
);

export default Breafing;

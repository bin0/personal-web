import React from 'react';
import styled from 'styled-components';
import { Typography } from '@mui/material';
import Navigation from 'components/Navigation';

const ToolsContainerStyled = styled.section`
  margin: 1rem;
`;

const Tools = () => {
  return (
    <ToolsContainerStyled>
      <Navigation back />
      <Typography variant="h1">Tools</Typography>
    </ToolsContainerStyled>
  );
};

export default Tools;

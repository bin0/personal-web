import React from 'react';
import PropTypes from 'prop-types';
import { DetailsListBoxContainerStyled } from '../styles';
import List from './List';

const DetailsList = ({ items, section }) => {
  return (
    <List title={section}>
      {items.map(({ title, list }) => (
        <DetailsListBoxContainerStyled key={title}>
          <li>{title}</li>
          <ul>
            {list.map((l) => (
              <li key={l}>{l}</li>
            ))}
          </ul>
        </DetailsListBoxContainerStyled>
      ))}
    </List>
  );
};

DetailsList.propTypes = {
  items: PropTypes.array.isRequired,
  section: PropTypes.string.isRequired,
};

export default DetailsList;

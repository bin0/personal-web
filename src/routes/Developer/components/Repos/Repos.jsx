import React, { useEffect, useState } from 'react';
import styled from 'styled-components';
import { Box, Grid, Grow, Link, Stack, Typography } from '@mui/material';
import Navigation from 'components/Navigation';
import GitHubIcon from '@mui/icons-material/GitHub';
import axios from 'axios';

const ReposContainerStyled = styled.section`
  margin: 1rem;
`;

const Repos = () => {
  const [gitlabRepos, setGitlabRepos] = useState([]);
  const [githubRepos, setGithubRepos] = useState([]);

  useEffect(() => {
    Promise.all([
      axios.get('https://api.github.com/users/Binomi0/repos'),
      axios.get('https://gitlab.com/api/v4/users/bin0/projects'),
    ]).then(([github, gitlab]) => {
      setGithubRepos(github.data);
      setGitlabRepos(gitlab.data);
    });
  }, []);

  return (
    <ReposContainerStyled>
      <Navigation back />
      <Box mt={4}>
        <Typography variant="h1">Repos</Typography>
      </Box>

      <Grid container spacing={2} mt={4}>
        <Grid item xs={12} sm={6} md={4} lg={3} xl={2}>
          <Stack direction="row" justifyContent="space-between">
            <Stack direction="row" spacing={2}>
              <GitHubIcon />
              <Link href="https://github.com/Binomi0" color="secondary">
                <Typography variant="overline">Github: @Binomi0</Typography>
              </Link>
            </Stack>
          </Stack>
          <Box p={2}>
            <Typography>Public</Typography>
            {githubRepos.length > 0 &&
              githubRepos.map((repo, index) => (
                <Box key={repo.id}>
                  <Link href={repo.html_url} color="secondary" target="_blank">
                    <Grow timeout={200 * (index + 1)} in unmountOnExit>
                      <Typography variant="caption">
                        {repo.full_name}
                      </Typography>
                    </Grow>
                  </Link>
                </Box>
              ))}
          </Box>
        </Grid>
        <Grid item xs={12} sm={6} md={4} lg={3} xl={2}>
          <Stack direction="row" justifyContent="space-between">
            <Link
              href="https://gitlab.com/bin0"
              color="secondary"
              target="_blank"
            >
              <Stack direction="row" spacing={2}>
                <img
                  width={36}
                  height={36}
                  src="/images/icons/gitlab-icon-rgb.svg"
                  alt="gitlab icon"
                />
                <Typography variant="overline">Gitlab: @bin0</Typography>
              </Stack>
            </Link>
          </Stack>
          <Box p={2}>
            <Typography>Public</Typography>
            {gitlabRepos.length > 0 &&
              gitlabRepos.map((repo, index) => (
                <Box key={repo.id}>
                  <Link href={repo.web_url} color="secondary" target="_blank">
                    <Grow timeout={200 * (index + 1)} in unmountOnExit>
                      <Typography variant="caption">{repo.name}</Typography>
                    </Grow>
                  </Link>
                </Box>
              ))}
          </Box>
        </Grid>
      </Grid>
    </ReposContainerStyled>
  );
};

export default Repos;

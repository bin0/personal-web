import React from 'react';
import { Route, Switch } from 'react-router-dom';
import * as Components from './components';
import Backend from './Backend';

const routes = [
  'Nodejs',
  'Express',
  'Bluebird',
  'Axios',
  'Dotenv',
  'Chalk',
  'JWT',
  'Session',
  'Lodash',
  'Moment',
  'MongoDB',
  'Mongoose',
  'NodeMailer',
  'Nodemon',
  'Javascript',
  'Test',
  'Eslint',
  'Prettier',
  'Istanbul',
];
const path = '/developer/backend';

const BackendRoutes = (props) => {
  return (
    <Switch>
      <Route exact path={path} component={Backend} />
      {routes.map((route) => (
        <Route
          key={route}
          exact
          path={`${path}/${route.toLowerCase()}`}
          component={Components[route]}
          {...props}
        />
      ))}
    </Switch>
  );
};

export default BackendRoutes;

import React from 'react';
import Typography from '@mui/material/Typography';
import { BackendContainerStyled } from '../../../styles';
import Navigation from 'components/Navigation';

const Test = () => (
  <BackendContainerStyled>
    <Navigation back />
    <Typography variant="h1">Test</Typography>
  </BackendContainerStyled>
);

export default Test;

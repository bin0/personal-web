import React from 'react';
import Typography from '@mui/material/Typography';
import { BackendContainerStyled } from '../../../styles';
import Navigation from 'components/Navigation';

const Session = () => (
  <BackendContainerStyled>
    <Navigation back />
    <Typography variant="h1">Session</Typography>
  </BackendContainerStyled>
);

export default Session;

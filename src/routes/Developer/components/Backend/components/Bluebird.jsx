import React from 'react';
import Typography from '@mui/material/Typography';
import { BackendContainerStyled } from '../../../styles';
import Navigation from 'components/Navigation';

const Bluebird = () => (
  <BackendContainerStyled>
    <Navigation back />
    <Typography variant="h1">Bluebird</Typography>
  </BackendContainerStyled>
);

export default Bluebird;

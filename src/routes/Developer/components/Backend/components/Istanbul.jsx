import React from 'react';
import Typography from '@mui/material/Typography';
import { BackendContainerStyled } from '../../../styles';
import Navigation from 'components/Navigation';

const Istanbul = () => (
  <BackendContainerStyled>
    <Navigation back />
    <Typography variant="h1">Istanbul</Typography>
  </BackendContainerStyled>
);

export default Istanbul;

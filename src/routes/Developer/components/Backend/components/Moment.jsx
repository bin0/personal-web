import React from 'react';
import Typography from '@mui/material/Typography';
import { BackendContainerStyled } from '../../../styles';
import Navigation from 'components/Navigation';

const Moment = () => (
  <BackendContainerStyled>
    <Navigation back />
    <Typography variant="h1">Moment</Typography>
  </BackendContainerStyled>
);

export default Moment;

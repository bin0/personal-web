import React from 'react';
import Typography from '@mui/material/Typography';
import { BackendContainerStyled } from '../../../styles';
import Navigation from 'components/Navigation';

const Eslint = () => (
  <BackendContainerStyled>
    <Navigation back />
    <Typography variant="h1">Eslint</Typography>
  </BackendContainerStyled>
);

export default Eslint;

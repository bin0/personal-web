import React from 'react';
import Typography from '@mui/material/Typography';
import { BackendContainerStyled } from '../../../styles';
import Navigation from 'components/Navigation';

const JWT = () => (
  <BackendContainerStyled>
    <Navigation back />
    <Typography variant="h1">JWT</Typography>
  </BackendContainerStyled>
);

export default JWT;

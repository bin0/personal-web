import React from 'react';
import Typography from '@mui/material/Typography';
import { BackendContainerStyled } from '../../../styles';
import Navigation from 'components/Navigation';

const Lodash = () => (
  <BackendContainerStyled>
    <Navigation back />
    <Typography variant="h1">Lodash</Typography>
  </BackendContainerStyled>
);

export default Lodash;

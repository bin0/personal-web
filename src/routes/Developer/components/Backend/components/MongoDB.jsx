import React from 'react';
import Typography from '@mui/material/Typography';
import { BackendContainerStyled } from '../../../styles';
import Navigation from 'components/Navigation';

const MongoDB = () => (
  <BackendContainerStyled>
    <Navigation back />
    <Typography variant="h1">MongoDB</Typography>
  </BackendContainerStyled>
);

export default MongoDB;

import React from 'react';
import Typography from '@mui/material/Typography';
import { BackendContainerStyled } from '../../../styles';
import Navigation from 'components/Navigation';

const Prettier = () => (
  <BackendContainerStyled>
    <Navigation back />
    <Typography variant="h1">Prettier</Typography>
  </BackendContainerStyled>
);

export default Prettier;

import React from 'react';
import Typography from '@mui/material/Typography';
import { BackendContainerStyled } from '../../../styles';
import Navigation from 'components/Navigation';

const Chalk = () => (
  <BackendContainerStyled>
    <Navigation back />
    <Typography variant="h1">Chalk</Typography>
  </BackendContainerStyled>
);

export default Chalk;

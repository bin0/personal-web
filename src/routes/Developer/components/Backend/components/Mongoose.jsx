import React from 'react';
import Typography from '@mui/material/Typography';
import { BackendContainerStyled } from '../../../styles';
import Navigation from 'components/Navigation';

const Mongoose = () => (
  <BackendContainerStyled>
    <Navigation back />
    <Typography variant="h1">Mongoose</Typography>
  </BackendContainerStyled>
);

export default Mongoose;

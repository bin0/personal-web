import React from 'react';
import Typography from '@mui/material/Typography';
import { BackendContainerStyled } from '../../../styles';
import Navigation from 'components/Navigation';

const Nodejs = () => (
  <BackendContainerStyled>
    <Navigation back />
    <Typography variant="h1">Nodejs</Typography>
  </BackendContainerStyled>
);

export default Nodejs;

import React from 'react';
import Typography from '@mui/material/Typography';
import { BackendContainerStyled } from '../../../styles';
import Navigation from 'components/Navigation';

const Nodemon = () => (
  <BackendContainerStyled>
    <Navigation back />
    <Typography variant="h1">Nodemon</Typography>
  </BackendContainerStyled>
);

export default Nodemon;

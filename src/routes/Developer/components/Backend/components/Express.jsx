import React from 'react';
import Typography from '@mui/material/Typography';
import { BackendContainerStyled } from '../../../styles';
import Navigation from 'components/Navigation';

const Express = () => (
  <BackendContainerStyled>
    <Navigation back />
    <Typography variant="h1">Express</Typography>
  </BackendContainerStyled>
);

export default Express;

import React from 'react';
import Typography from '@mui/material/Typography';
import Navigation from 'components/Navigation';
import { BackendContainerStyled } from 'routes/Developer/styles';

const Axios = () => (
  <BackendContainerStyled>
    <Navigation back />
    <Typography variant="h1">Axios</Typography>
  </BackendContainerStyled>
);

export default Axios;

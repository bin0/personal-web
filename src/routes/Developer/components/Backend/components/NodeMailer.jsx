import React from 'react';
import Typography from '@mui/material/Typography';
import { BackendContainerStyled } from '../../../styles';
import Navigation from 'components/Navigation';

const NodeMailer = () => (
  <BackendContainerStyled>
    <Navigation back />
    <Typography variant="h1">NodeMailer</Typography>
  </BackendContainerStyled>
);

export default NodeMailer;

import React from 'react';
import Typography from '@mui/material/Typography';
import Grid from '@mui/material/Grid';
import Card from 'components/Card';
import { BackendContainerStyled } from '../../styles';
import Navigation from 'components/Navigation';

const Backend = () => (
  <BackendContainerStyled>
    <Navigation back />
    <Typography variant="h1">Backend</Typography>
    <Grid container justifyContent="center" spacing={2} direction="row">
      <Grid item xs={6} sm={4} md={3}>
        <Card route="backend" title="NodeJS" subheader="Main Language" />
        <Card route="backend" title="Express" subheader="Node Framework" />
        <Card route="backend" title="Bluebird" subheader="Promise Library" />
        <Card route="backend" title="Axios" subheader="HTTP/S requests" />
        <Card route="backend" title="Chalk" subheader="Console colors" />
      </Grid>

      <Grid item xs={6} sm={4} md={3}>
        <Card route="backend" title="Dotenv" subheader="Env vars" />
        <Card route="backend" title="JWT" subheader="Auth Tokens" />
        <Card route="backend" title="Session" subheader="Express session" />
        <Card route="backend" title="Lodash" subheader="Utils library" />
        <Card route="backend" title="Moment" subheader="Dates library" />
      </Grid>
      <Grid item xs={6} sm={4} md={3}>
        <Card route="backend" title="MongoDB" subheader="Database" />
        <Card route="backend" title="Mongoose" subheader="ORM" />
        <Card route="backend" title="NodeMailer" subheader="Mail server" />
        <Card route="backend" title="Nodemon" subheader="Dev server" />
      </Grid>
      <Grid item xs={6} sm={4} md={3}>
        <Card route="backend" title="Javascript" subheader="ES6" />
        <Card route="backend" title="Test" subheader="Unit Test" />
        <Card route="backend" title="Eslint" subheader="Code quality" />
        <Card route="backend" title="Prettier" subheader="Code format" />
        <Card route="backend" title="Istanbul" subheader="Test Asserts" />
      </Grid>
    </Grid>
  </BackendContainerStyled>
);

export default Backend;

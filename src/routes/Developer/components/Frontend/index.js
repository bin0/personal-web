import React from 'react';
import { Route, Switch } from 'react-router-dom';
import * as Components from './components';
import Frontend from './Frontend';
import { renderLazyComponent } from 'utils/components';

const routes = [
  'Reactjs',
  'ReactRouter',
  'Redux',
  'Materialui',
  'Socketio',
  'HTML5',
  'CSS3',
  'Victory',
  'Firebase',
  'Cypress',
  'Jest',
  'Enzyme',
  'StyledComponents',
  'Sass',
  'Javascript',
];
const path = '/developer/frontend';

const FrontendRoutes = (props) => {
  return (
    <Switch>
      <Route exact path={path} component={Frontend} />
      {routes.map((route) => (
        <Route
          key={route}
          path={`${path}/${route.toLowerCase()}`}
          render={(props) => renderLazyComponent(Components[route], props)}
          exact
          {...props}
        />
      ))}
    </Switch>
  );
};

export default FrontendRoutes;

import React from 'react';
import { Helmet } from 'react-helmet';
import Typography from '@mui/material/Typography';
import Grid from '@mui/material/Grid';
import Card from 'components/Card';
import Navigation from 'components/Navigation';
import Breafing from './Breafing';
import { FrontendContainerStyled } from '../../styles';

const Frontend = () => (
  <FrontendContainerStyled>
    <Helmet>
      <title>Frontend | Senior FullStack Developer</title>
    </Helmet>
    <Navigation back />
    <Typography variant="h1">Frontend</Typography>
    <Breafing />
    <Grid container spacing={1} direction="row">
      <Grid item xs={12} sm={6} md={4} xl={3}>
        <Card route="frontend" title="ReactJS" subheader="Framework JS" />
        <Card route="frontend" title="ReactRouter" subheader="Navigation" />
        <Card route="frontend" title="Redux" subheader="App state" />
        <Card route="frontend" title="Socketio" subheader="Websockets" />
      </Grid>

      <Grid item xs={12} sm={6} md={4} xl={3}>
        <Card route="frontend" title="Firebase" subheader="Utils" />
        <Card route="frontend" title="Cypress" subheader="E2E Test" />
        <Card route="frontend" title="Jest" subheader="Unit Test" />
        <Card route="frontend" title="Enzyme" subheader="Component Test" />
      </Grid>
      <Grid item xs={12} sm={6} md={4} xl={3}>
        <Card route="frontend" title="MaterialUI" subheader="Web components" />
        <Card route="frontend" title="Bootstrap" subheader="Web components" />
        <Card route="frontend" title="StyledComponents" subheader="Styles" />
        <Card route="frontend" title="Sass" subheader="Advanced Styling" />
      </Grid>
      <Grid item xs={12} sm={6} md={4} xl={3}>
        <Card route="frontend" title="Victory" subheader="Graphs" />
        <Card route="frontend" title="CSS3" subheader="Basic Styling" />
        <Card route="frontend" title="HTML5" subheader="W3C" />
        <Card route="frontend" title="Javascript" subheader="ES6" />
      </Grid>
    </Grid>
  </FrontendContainerStyled>
);

export default Frontend;

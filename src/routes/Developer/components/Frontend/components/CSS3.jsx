import React from 'react';
import Typography from '@mui/material/Typography';
import { BackendContainerStyled } from '../../../styles';
import Navigation from 'components/Navigation';

const CSS3 = (props) => (
  <BackendContainerStyled>
    <Navigation back />
    <Typography variant="h1">CSS3</Typography>
  </BackendContainerStyled>
);

export default CSS3;

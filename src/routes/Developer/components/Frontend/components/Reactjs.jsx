import React from 'react';
import Typography from '@mui/material/Typography';
import Link from '@mui/material/Link';
import Avatar from '@mui/material/Avatar';
import makeStyles from '@mui/styles/makeStyles';
import Card from '@mui/material/Card';
import CardHeader from '@mui/material/CardHeader';
import CardMedia from '@mui/material/CardMedia';
import CardContent from '@mui/material/CardContent';
import IconButton from '@mui/material/IconButton';
import { red } from '@mui/material/colors';
import MoreVertIcon from '@mui/icons-material/MoreVert';
import {
  FrontendContainerStyled,
  FrontendHeaderStyled,
  LinkStyled,
} from '../../../styles';
import Navigation from 'components/Navigation';
import logos from '../../../logos';
import reactAlicanteImg from 'assets/img/react-alicante.jpg';
import codeSplitingImg from 'assets/screenshots/code-spliting.jpg';

const useStyles = makeStyles((theme) => ({
  root: {
    maxWidth: 345,
  },
  media: {
    height: 0,
    paddingTop: '56.25%', // 16:9
  },
  expand: {
    transform: 'rotate(0deg)',
    marginLeft: 'auto',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
  },
  expandOpen: {
    transform: 'rotate(180deg)',
  },
  avatar: {
    backgroundColor: red[500],
  },
}));

const Reactjs = () => {
  const classes = useStyles();

  return (
    <FrontendContainerStyled>
      <Navigation back />
      <FrontendHeaderStyled>
        <Avatar src={logos.frontend} />
        <Typography variant="h1">ReactJS</Typography>
      </FrontendHeaderStyled>
      <div>
        <Typography variant="h2">Senior Frontend Developer</Typography>
        <Typography paragraph>
          To be honest, as a <b>Senior Frontend Developer</b>, this is my
          favorite framework to work with. It was in 2016 when I first knew
          about it, became my main focus to master.
        </Typography>
        <Typography paragraph style={{ textAlign: 'justify' }}>
          I was given the opportunity to work with big companies like{' '}
          <b>INDITEX</b> or the award winning startup like <b>Fintonic</b>. At
          that time, I meet wonderful people who helped me to grow really
          quickly. I learned from Angel Cereijo, Jorge Queiruga, Javier
          Bermúdez, Matias Delgado, Luisma, Nico... Every one with great
          individual skills. <b>So I can't be more grateful</b>.
        </Typography>
        <Typography paragraph>
          In those years, I <b>started projects from scratch to production</b>,
          I also worked with many API's, like Google, Facebook, Coinbase, and
          many others, not only public but also private ones.
        </Typography>
        <Typography variant="h2">What did I do with React?</Typography>
        <Typography paragraph>
          In the <LinkStyled to="/developer/repos">repos section </LinkStyled>
          you can see all my public repos.
        </Typography>
        <Typography paragraph style={{ textAlign: 'justify' }}>
          <Link
            color="secondary"
            href="https://gitlab.com/onrubia78/salvamento"
            target="_blank"
            rel="noopener noreferer"
            underline="hover"
          >
            GBPD{' '}
          </Link>
          is a non-profit project that started{' '}
          <b>in order to help finding a missing person</b> throughout the search
          groups. The objective here is to create an application so that they
          know in real-time where others are, what areas were searched, what
          areas should be reviewed and so on. It is public and anyone can PR at
          any time, <b>any commit will be highly appreciated.</b>
        </Typography>
        <Typography paragraph>
          One of the things I usually do is `refactoring`, move from class
          components to functional components. I <b>Upgraded legacy projects</b>{' '}
          starting with React 15.x to the latest version succesfully.
        </Typography>
        <div>
          <Typography variant="h2">
            React techiques used on this site
          </Typography>
          <Typography variant="h2">CODE SPLITING</Typography>
          <a target="_blank" href="/assets/media/code-spliting.jpg">
            <img
              src={codeSplitingImg}
              alt="code-spliting"
              title="Code Spliting"
            />
          </a>

          <Typography variant="h2">Testing React components</Typography>
          <code>
            describe('renderLazyComponent(Component, props)', () =>{' '}
            {`{
    test('should return a component', () => {
      const C = () => <div>Test</div>;
      const wrapper = mount(renderLazyComponent(C));

      expect(wrapper.html()).toBe('<div>Test</div>');
    });
  });`}
          </code>
        </div>
        Official docs{' '}
        <Link
          color="secondary"
          href="https://reactjs.org/"
          target="_blank"
          rel="noopener noreferer"
          underline="hover"
        >
          reactjs
        </Link>
      </div>
      <div>
        <Card className={classes.root}>
          <CardHeader
            avatar={
              <Avatar aria-label="recipe" className={classes.avatar}>
                RA
              </Avatar>
            }
            action={
              <IconButton aria-label="settings" size="large">
                <MoreVertIcon />
              </IconButton>
            }
            title="React Alicante"
            subheader="26-28 de Septiembre de 2019"
          />
          <CardMedia
            className={classes.media}
            image={reactAlicanteImg}
            title="React Alicante"
          />
          <CardContent>
            <Typography component="p">
              React Alicante is a yearly event that joins hundreds of
              developers. I had a very good time there with all the conferences.
              The place, Melia Hotel is awesome. Can't wait to get the next one!
            </Typography>
          </CardContent>
        </Card>
      </div>
    </FrontendContainerStyled>
  );
};

export default Reactjs;

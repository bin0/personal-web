import React from 'react';

export const Reactjs = React.lazy(() => import('./Reactjs'));
export const Bootstrap = React.lazy(() => import('./Bootstrap'));
export const CSS3 = React.lazy(() => import('./CSS3'));
export const Cypress = React.lazy(() => import('./Cypress'));
export const Firebase = React.lazy(() => import('./Firebase'));
export const HTML5 = React.lazy(() => import('./HTML5'));
export const Javascript = React.lazy(() => import('./Javascript'));
export const Jest = React.lazy(() => import('./Jest'));
export const Materialui = React.lazy(() => import('./Materialui'));
export const ReactRouter = React.lazy(() => import('./ReactRouter'));
export const Redux = React.lazy(() => import('./Redux'));
export const Sass = React.lazy(() => import('./Sass'));
export const Socketio = React.lazy(() => import('./Socketio'));
export const StyledComponents = React.lazy(() => import('./StyledComponents'));
export const Victory = React.lazy(() => import('./Victory'));

import React from 'react';
import Typography from '@mui/material/Typography';
import { BackendContainerStyled } from '../../../styles';
import Navigation from 'components/Navigation';

const ReactRouter = () => (
  <BackendContainerStyled>
    <Navigation back />
    <Typography variant="h1">React Router</Typography>
  </BackendContainerStyled>
);

export default ReactRouter;

import React from 'react';
import Typography from '@mui/material/Typography';
import { BackendContainerStyled } from '../../../styles';
import Navigation from 'components/Navigation';

const StyledComponents = () => (
  <BackendContainerStyled>
    <Navigation back />
    <Typography variant="h1">Styled Components</Typography>
  </BackendContainerStyled>
);

export default StyledComponents;

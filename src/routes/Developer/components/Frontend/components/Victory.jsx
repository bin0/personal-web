import React from 'react';
import Typography from '@mui/material/Typography';
import { BackendContainerStyled } from '../../../styles';
import Navigation from 'components/Navigation';

const Victory = () => (
  <BackendContainerStyled>
    <Navigation back />
    <Typography variant="h1">Victory</Typography>
  </BackendContainerStyled>
);

export default Victory;

import React from 'react';
import Typography from '@mui/material/Typography';
import { BackendContainerStyled } from '../../../styles';
import Navigation from 'components/Navigation';

const Socketio = () => (
  <BackendContainerStyled>
    <Navigation back />
    <Typography variant="h1">Socket.io</Typography>
  </BackendContainerStyled>
);

export default Socketio;

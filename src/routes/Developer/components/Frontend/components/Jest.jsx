import React from 'react';
import Typography from '@mui/material/Typography';
import { BackendContainerStyled } from '../../../styles';
import Navigation from 'components/Navigation';

const Jest = () => (
  <BackendContainerStyled>
    <Navigation back />
    <Typography variant="h1">Jest</Typography>
  </BackendContainerStyled>
);

export default Jest;

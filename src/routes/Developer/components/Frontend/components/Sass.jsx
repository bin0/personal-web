import React from 'react';
import Typography from '@mui/material/Typography';
import { BackendContainerStyled } from '../../../styles';
import Navigation from 'components/Navigation';

const Sass = () => (
  <BackendContainerStyled>
    <Navigation back />
    <Typography variant="h1">Sass</Typography>
  </BackendContainerStyled>
);

export default Sass;

import React from 'react';
import Typography from '@mui/material/Typography';
import { BackendContainerStyled } from '../../../styles';
import Navigation from 'components/Navigation';

const Javascript = () => (
  <BackendContainerStyled>
    <Navigation back />
    <Typography variant="h1">Javascript</Typography>
  </BackendContainerStyled>
);

export default Javascript;

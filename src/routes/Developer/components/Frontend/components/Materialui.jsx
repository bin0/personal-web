import React from 'react';
import Typography from '@mui/material/Typography';
import { BackendContainerStyled } from '../../../styles';
import Navigation from 'components/Navigation';

const Materialui = () => (
  <BackendContainerStyled>
    <Navigation back />
    <Typography variant="h1">Material UI</Typography>
  </BackendContainerStyled>
);

export default Materialui;

import React from 'react';
import Typography from '@mui/material/Typography';
import { BackendContainerStyled } from '../../../styles';
import Navigation from 'components/Navigation';

const Redux = () => (
  <BackendContainerStyled>
    <Navigation back />
    <Typography variant="h1">Redux</Typography>
  </BackendContainerStyled>
);

export default Redux;

import React from 'react';
import Typography from '@mui/material/Typography';
import { BackendContainerStyled } from '../../../styles';
import Navigation from 'components/Navigation';

const Bootstrap = () => (
  <BackendContainerStyled>
    <Navigation back />
    <Typography variant="h1">Bootstrap</Typography>
  </BackendContainerStyled>
);

export default Bootstrap;

import React from 'react';
import Typography from '@mui/material/Typography';
import { BackendContainerStyled } from '../../../styles';
import Navigation from 'components/Navigation';

const Enzyme = () => (
  <BackendContainerStyled>
    <Navigation back />
    <Typography variant="h1">Enzyme</Typography>
  </BackendContainerStyled>
);

export default Enzyme;

import React from 'react';
import Typography from '@mui/material/Typography';
import { BackendContainerStyled } from '../../../styles';
import Navigation from 'components/Navigation';

const Cypress = () => (
  <BackendContainerStyled>
    <Navigation back />
    <Typography variant="h1">Cypress</Typography>
  </BackendContainerStyled>
);

export default Cypress;

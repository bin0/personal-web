import React from 'react';
import Typography from '@mui/material/Typography';
import { BackendContainerStyled } from '../../../styles';
import Navigation from 'components/Navigation';

const Firebase = () => (
  <BackendContainerStyled>
    <Navigation back />
    <Typography variant="h1">Firebase</Typography>
  </BackendContainerStyled>
);

export default Firebase;

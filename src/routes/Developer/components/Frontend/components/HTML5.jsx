import React from 'react';
import Typography from '@mui/material/Typography';
import { BackendContainerStyled } from '../../../styles';
import Navigation from 'components/Navigation';

const HTML5 = () => (
  <BackendContainerStyled>
    <Navigation back />
    <Typography variant="h1">HTML5</Typography>
  </BackendContainerStyled>
);

export default HTML5;

import React from 'react';
import { Typography } from '@mui/material';

const Breafing = () => {
  return (
    <Typography paragraph>
      Cuando tienes la suerte o la valentía de elegir hacer lo que realmente
      quieres hacer en la vida, empiezas a ver las cosas de diferente manera.
    </Typography>
  );
};

export default Breafing;

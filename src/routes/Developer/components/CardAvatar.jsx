import React from 'react';
import PropTypes from 'prop-types';
import { Avatar } from '@mui/material';
import makeStyles from '@mui/styles/makeStyles';
import Help from '@mui/icons-material/Help';
import logos from '../logos';
import { icons } from '../icons';

const useStyles = makeStyles((theme) => ({
  primary: {
    backgroundColor: theme.palette.primary.main,
  },
}));

const CardAvatar = ({ item }) => {
  const classes = useStyles();

  const logo = logos[item];
  if (logo) {
    return <Avatar src={logo} />;
  }

  const Icon = icons[item];
  if (Icon) {
    return (
      <Avatar className={classes.primary}>
        <Icon color="secondary" />
      </Avatar>
    );
  }

  return (
    <Avatar>
      <Help />
    </Avatar>
  );
};

CardAvatar.defaultProps = {
  item: null,
};

CardAvatar.propTypes = {
  item: PropTypes.string,
};

export default CardAvatar;

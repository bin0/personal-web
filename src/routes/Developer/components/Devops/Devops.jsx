import React from 'react';
import styled from 'styled-components';
import { Typography } from '@mui/material';
import Navigation from 'components/Navigation';

const DevopsContainerStyled = styled.section`
  margin: 1rem;
`;

const Devops = () => {
  return (
    <DevopsContainerStyled>
      <Navigation back />
      <Typography variant="h1">DevOps</Typography>
    </DevopsContainerStyled>
  );
};

export default Devops;

import BuildIcon from '@mui/icons-material/Build';
import CodeIcon from '@mui/icons-material/Code';
import EmojiPeopleIcon from '@mui/icons-material/EmojiPeople';

export const icons = {
  tools: BuildIcon,
  repos: CodeIcon,
  social: EmojiPeopleIcon,
};

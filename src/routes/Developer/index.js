import React, { Suspense } from 'react';
import { Route, Switch } from 'react-router-dom';
import DeveloperContainer from './containers/DeveloperContainer';
import backendRoutes from './components/Backend';
import frontendRoutes from './components/Frontend';
import LoadingBar from 'react-redux-loading-bar';
import gAnalitycs from 'utils/analitycs/google-analitycs';

const Frontend = React.lazy(() => import('./components/Frontend'));
const Backend = React.lazy(() => import('./components/Backend'));
const Devops = React.lazy(() => import('./components/Devops'));
const Repos = React.lazy(() => import('./components/Repos'));
const Tools = React.lazy(() => import('./components/Tools'));
const Social = React.lazy(() => import('./components/Social'));

const DeveloperRoutes = (props) => {
  window.scrollTo(0, 0);
  gAnalitycs.pageview(props.location.pathname);
  return (
    <Switch>
      <Suspense
        fallback={<LoadingBar className="redux-loading-bar" showFastActions />}
      >
        <Route exact path="/developer" component={DeveloperContainer} />
        <Route path="/developer/frontend" component={Frontend}>
          {frontendRoutes}
        </Route>
        <Route path="/developer/backend" component={Backend}>
          {backendRoutes}
        </Route>
        <Route exact path="/developer/devops" component={Devops} />
        <Route exact path="/developer/repos" component={Repos} />
        <Route exact path="/developer/tools" component={Tools} />
        <Route exact path="/developer/social" component={Social} />
      </Suspense>
    </Switch>
  );
};

export default DeveloperRoutes;

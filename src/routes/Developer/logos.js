import frontend from '../../assets/img/logo192.png';
import backend from '../../assets/img/logos/nodejs-new-pantone-black.png';
import devops from '../../assets/img/logos/Moby-logo.png';

const logos = {
  frontend,
  backend,
  devops,
};

export default logos;

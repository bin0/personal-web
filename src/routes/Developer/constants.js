export const frontend = [
  {
    title: 'ReactJS',
    list: [
      'React Router',
      'Redux',
      'Material UI',
      'Styled Components',
      'Socket.io',
      'Victory',
      'Firebase',
      'Cypress',
      'Jest - Enzyme',
    ],
  },
  {
    title: 'HTML5',
    list: [],
  },
  {
    title: 'CSS3',
    list: [],
  },
  {
    title: 'Sass',
    list: [],
  },
  {
    title: 'Javascript',
    list: [],
  },
];

export const backend = [
  { title: 'NodeJS', list: [] },
  { title: 'Express', list: [] },
  { title: 'MongoDB', list: [] },
  { title: 'MYSQL', list: [] },
  { title: 'Postgress', list: [] },
];

export const devops = [
  { title: 'Docker', list: [] },
  { title: 'Kubernetes', list: [] },
  { title: 'Gitlab-CI', list: [] },
  { title: 'Deploys', list: ['Firebase', 'Heroku', 'Now'] },
  { title: 'Sentry', list: [] },
  { title: 'SonarQube', list: [] },
];

export const tools = [
  { title: 'MacOS - Linux - Windows', list: [] },
  { title: 'VSCode - WebStorm', list: [] },
  { title: 'Postman', list: [] },
  { title: 'SourceTree', list: [] },
];
export const repos = [
  { title: 'Github', list: [] },
  { title: 'Gitlab', list: [] },
];
export const social = [
  { title: 'StackOverflow EN', list: [] },
  { title: 'StackOverflow ES', list: [] },
];

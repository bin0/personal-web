import React from 'react';
import { mount } from 'enzyme';
import toJSON from 'enzyme-to-json';
import CardAvatar from '../components/CardAvatar';

describe('<CardAvatar />', () => {
  describe('Component rendering', () => {
    it('should match snapshot', () => {
      const renderWrapper = mount(<CardAvatar />);
      expect(toJSON(renderWrapper)).toMatchSnapshot();
    });

    it('should have default icon', () => {
      const wrapper = mount(<CardAvatar />);

      const title = wrapper.find('svg');

      expect(title).toHaveLength(1);
    });

    it('should have custom image', () => {
      const wrapper = mount(<CardAvatar item="frontend" />);

      const title = wrapper.find('img');

      expect(title).toHaveLength(1);
    });

    it('should have custom icon', () => {
      const wrapper = mount(<CardAvatar item="social" />);

      const title = wrapper.find('svg');

      expect(title).toHaveLength(1);
    });
  });
});

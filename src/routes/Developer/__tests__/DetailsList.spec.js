import React from 'react';
import { shallow } from 'enzyme';
import toJSON from 'enzyme-to-json';
import DetailsList from '../components/DetailsList';

jest.mock('react-router-dom');

describe('<DeveloperView />', () => {
  const items = [
    { title: 'Test', list: [] },
    { title: 'Test with list', list: ['list item'] },
  ];
  describe('Shallow rendering', () => {
    it('should match snapshot', () => {
      const renderWrapper = shallow(
        <DetailsList items={items} section="Test" />
      );
      expect(toJSON(renderWrapper)).toMatchSnapshot();
    });

    it('should have a title', () => {
      const wrapper = shallow(<DetailsList items={items} section="Test" />);

      const title = wrapper.find('li');

      expect(title).toHaveLength(3);
    });
  });
});

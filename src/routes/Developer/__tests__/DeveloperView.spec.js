import React from 'react';
import { shallow } from 'enzyme';
import DeveloperView from '../components/DeveloperView';

describe('<DeveloperView />', () => {
  describe('Shallow rendering', () => {
    it('should match snapshot', () => {
      const renderWrapper = shallow(<DeveloperView />);
      expect(renderWrapper.text()).toMatchSnapshot();
    });
  });
});

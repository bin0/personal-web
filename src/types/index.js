const TOKEN_ADDRESS = {
  OVR: {
    L1: '0x21bfbda47a0b4b5b1248c767ee49f7caa9b23697',
    L2: '',
  },
  DAI: {
    L1: 'dai.tokens.ethers.eth',
    L2: '',
  },
  ETH: {
    L1: 'ethers.eth',
    L2: '',
  },
  WBTC: {
    L1: '0x2260fac5e5542a773aa44fbcfedf7c193bc2c599',
    L2: '',
  },
  LINK: {
    L1: '0x514910771af9ca656af840dff83e8264ecf986ca',
    L2: '',
  },
  GRT: {
    L1: '0xc944e90c64b2c07662a292be6244bdf05cda44a7',
    L2: '',
  },
  MATIC: {
    L1: '0x7d1afa7b718fb893db30a3abc0cfc608aacfebb0',
    L2: '',
  },
};

const tokenFactory = (symbol, name, network, decimals) => ({
  name,
  symbol,
  address: TOKEN_ADDRESS[symbol][network],
  decimals,
});

const tokens = {
  L1: [
    tokenFactory('OVR', 'OVR', 'L1', 18),
    tokenFactory('DAI', 'DAI', 'L1', 18),
    tokenFactory('ETH', 'ETH', 'L1', 18),
    tokenFactory('WBTC', 'WBTC', 'L1', 8),
    tokenFactory('LINK', 'LINK', 'L1', 18),
    tokenFactory('GRT', 'GRT', 'L1', 18),
    tokenFactory('MATIC', 'MATIC', 'L1', 18),
  ],
  L2: [],
};

export const chains = [
  {
    id: 1088,
    name: 'METIS NETWORK',
  },
  {
    id: 1,
    name: 'ETHEREUM MAINNET',
  },
  {
    id: 137,
    name: 'MATIC NETWORK',
  },
];

export const getChainName = (id) => chains.find((c) => c.id === id).name;

export default tokens;

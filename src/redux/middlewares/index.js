import { loadingBarMiddleware } from 'react-redux-loading-bar';
import thunkMiddleware from 'redux-thunk';

const promiseTypeSuffixes = ['REQUEST', 'SUCCESS', 'FAILED'];

const middlewares = [
  thunkMiddleware,
  loadingBarMiddleware({ promiseTypeSuffixes }),
];

export default middlewares;

import { applyMiddleware, createStore } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension/developmentOnly';

import customReduxMiddlewares from './middlewares';
import rootReducer from '../reducers';

const store = (initialState = {}) => {
  const middlewares = [...customReduxMiddlewares];
  let enhancers = applyMiddleware(...middlewares);

  // Redux Dev Tools
  if (process.env.NODE_ENV === 'development') {
    const composeEnhancers = composeWithDevTools({});
    enhancers = composeEnhancers(applyMiddleware(...middlewares));
  }

  const store = createStore(rootReducer, initialState, enhancers);

  if (module.hot) {
    module.hot.accept('../reducers', () => {
      const reducers = require('../reducers').default;
      store.replaceReducer(reducers(store.asyncReducers));
    });
  }

  return store;
};
export default store;

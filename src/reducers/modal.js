import createReducer from '../redux/create-reducer';

import { OPEN_MODAL, CLOSE_MODAL } from '../action-types';

const openModal =
  ({ modalType, modalProps }) =>
  (dispatch) => {
    dispatch({ type: OPEN_MODAL.SET, payload: { modalType, modalProps } });
  };
const closeModal = () => (dispatch) => {
  dispatch({ type: CLOSE_MODAL.SET });
};

export const actions = {
  openModal,
  closeModal,
};

const INITIAL_STATE = {
  modalType: '',
  modalProps: {},
  open: false,
};

const ACTION_HANDLER = {
  [OPEN_MODAL.SET]: (_, { payload }) => ({ ...payload, open: true }),
  [CLOSE_MODAL.SET]: () => INITIAL_STATE,
};

export default createReducer(INITIAL_STATE, ACTION_HANDLER);

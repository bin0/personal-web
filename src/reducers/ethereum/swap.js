import createReducer from 'redux/create-reducer';

export const actions = {};

const INITIAL_STATE = {};

const ACTION_HANDLERS = {
  ACTION: (state, { payload }) => ({
    ...state,
    ...payload,
  }),
};

export default createReducer(INITIAL_STATE, ACTION_HANDLERS);

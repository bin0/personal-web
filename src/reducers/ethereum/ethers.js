import createReducer from 'redux/create-reducer';
import { ethers } from 'ethers';
import { SET_ETHERS } from 'action-types';

const initEthers = () => (dispatch, getState) => {
  const currentEthers = getState().ethereum.ethers;
  if (!currentEthers.provider) {
    try {
      const provider = new ethers.providers.Web3Provider(
        window.ethereum,
        'any'
      );

      if (!provider) {
        dispatch({ type: SET_ETHERS.FAILURE, payload: 'NO PROVIDER' });
        return;
      }

      const signer = provider.getSigner();
      const etherscanProvider = new ethers.providers.EtherscanProvider();

      dispatch({
        type: SET_ETHERS.SET,
        payload: { provider, signer, etherscanProvider },
      });
    } catch (error) {
      dispatch({ type: SET_ETHERS.FAILURE, payload: error });
    }
  } else {
    dispatch({
      type: SET_ETHERS.SET,
      payload: currentEthers,
    });
  }
};

export const actions = { initEthers };

const INITIAL_STATE = {
  signer: null,
  provider: null,
  etherscanProvider: null,
  error: null,
};

const ACTION_HANDLERS = {
  [SET_ETHERS.REQUEST]: () => INITIAL_STATE,
  [SET_ETHERS.SET]: (state, { payload }) => ({ ...state, ...payload }),
  [SET_ETHERS.FAILURE]: (state, { payload }) => ({ ...state, error: payload }),
};

export const getEthersErrorSelector = (state) => state.ethereum.ethers.error;
export default createReducer(INITIAL_STATE, ACTION_HANDLERS);

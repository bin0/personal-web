import { combineReducers } from 'redux';
import ethersReducer from './ethers';
import networkReducer from './network';
import tokensReducer from './tokens';
import walletReducer from './wallet';
import swapReducer from './swap';

export default combineReducers({
  ethers: ethersReducer,
  network: networkReducer,
  tokens: tokensReducer,
  wallet: walletReducer,
  swap: swapReducer,
});

import { WALLET_ADDRESS } from 'action-types';
import { hideLoading, showLoading } from 'react-redux-loading-bar';
import createReducer from 'redux/create-reducer';

const getWalletAddress = () => async (dispatch, getState) => {
  const { signer, provider } = getState().ethereum.ethers;

  if (!signer || !provider) return;

  try {
    dispatch(showLoading());
    await provider.send('eth_requestAccounts', []);

    const address = await signer.getAddress();

    dispatch({ type: WALLET_ADDRESS.SET, payload: address });
  } catch (error) {
    dispatch({ type: WALLET_ADDRESS.FAILURE, payload: error });
  } finally {
    dispatch(hideLoading());
  }
};

export const actions = { getWalletAddress };

const INITIAL_STATE = {
  walletAddress: '',
};

const ACTION_HANDLERS = {
  [WALLET_ADDRESS.SET]: (state, { payload }) => ({
    ...state,
    walletAddress: payload,
  }),
};

export default createReducer(INITIAL_STATE, ACTION_HANDLERS);

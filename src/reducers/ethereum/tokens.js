import createReducer from 'redux/create-reducer';
import { Contract, utils } from 'ethers';
import { hideLoading, showLoading } from 'react-redux-loading-bar';
import { GET_TOKENS } from 'action-types';
import tokens from 'types';
import { findBySymbol } from 'utils/arrays';
import genericErc20Abi from '../../config/ethereum/erc20-abi.json';

/**
 * @description It gets the main network balance of the connected blockchain
 * could be ETH, MATIC, METIS, ETH(arbitrum) ETH(polygon) and so on...
 * @returns void
 */
const getEthBalance = () => async (dispatch, getState) => {
  const { provider, signer } = getState().ethereum.ethers;
  if (!provider) {
    return;
  }
  const balance = await provider.getBalance(signer.getAddress());
  const network = getState().ethereum.network || (await provider.getNetwork());

  const networkToken = () => {
    switch (network.chainId) {
      case 1:
        return 'ETH';
      case 137:
        return 'MATIC';
      case 1088:
        return 'METIS';
      default:
        return '';
    }
  };
  const token = tokens.L1.find((t) => t.symbol === networkToken);

  dispatch({
    type: GET_TOKENS.SET,
    payload: {
      [networkToken()]: {
        token,
        balance: utils.formatEther(balance),
      },
    },
  });
};

const getTokenBalance =
  (symbols = []) =>
  async (dispatch, getState) => {
    const { provider, signer } = getState().ethereum.ethers;

    if (!provider) {
      return;
    }

    symbols.forEach(async (symbol) => {
      const token = tokens.L1.find(findBySymbol(symbol));
      if (!token) return;

      try {
        dispatch(showLoading());
        const contract = new Contract(token.address, genericErc20Abi, provider);
        const balance = await contract.balanceOf(signer.getAddress());

        dispatch({
          type: GET_TOKENS.SET,
          payload: {
            [symbol]: {
              token,
              balance: utils.formatUnits(balance, token.decimals),
            },
          },
        });
      } catch (error) {
        dispatch({ type: GET_TOKENS.FAILURE });
      } finally {
        dispatch(hideLoading());
      }
    });
  };

export const actions = { getEthBalance, getTokenBalance };

const INITIAL_STATE = {
  ETH: null,
  OVR: null,
  DAI: null,
  WBTC: null,
};

const ACTION_HANDLERS = {
  [GET_TOKENS.SET]: (state, { payload }) => ({ ...state, ...payload }),
};

export default createReducer(INITIAL_STATE, ACTION_HANDLERS);

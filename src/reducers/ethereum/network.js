import { ETH_NETWORK } from 'action-types';
import { utils } from 'ethers';
import { hideLoading, showLoading } from 'react-redux-loading-bar';
import createReducer from 'redux/create-reducer';
import { actions as snackActions } from 'reducers/snack';

const errors = {
  networkError: 'network/connection-error',
};

const getNetwork = () => async (dispatch, getState) => {
  const { provider } = getState().ethereum.ethers;

  if (!provider) return;
  try {
    dispatch(showLoading());
    const network = await provider.getNetwork();

    dispatch(
      snackActions.openSnack({
        message: `Connected netword: ${network.name} - chainId: ${network.chainId}`,
        snackProps: { severity: 'success' },
      })
    );
    dispatch({ type: ETH_NETWORK.SET, payload: network });
  } catch (error) {
    dispatch({ type: ETH_NETWORK.FAILURE });
  } finally {
    dispatch(hideLoading());
  }
};

const changeNetwork = (networkId) => async (dispatch, getState) => {
  const { provider } = getState().ethereum.ethers;

  try {
    dispatch(showLoading());
    await provider.send('wallet_switchEthereumChain', [
      { chainId: utils.hexValue(networkId) },
    ]);
    window.location.reload();
  } catch (error) {
    dispatch({ type: ETH_NETWORK.FAILURE });
  } finally {
    dispatch(hideLoading());
  }
};

export const actions = { getNetwork, changeNetwork };

const INITIAL_STATE = {
  chainId: 0,
  name: '',
  ensAddress: '',
  error: null,
};

const ACTION_HANDLERS = {
  [ETH_NETWORK.SET]: (state, { payload }) => ({
    ...state,
    ...payload,
  }),
  [ETH_NETWORK.FAILURE]: (state) => ({ ...state, error: errors.networkError }),
};

export const getChainIdSelector = (state) => state.ethereum.network.chainId;
export default createReducer(INITIAL_STATE, ACTION_HANDLERS);

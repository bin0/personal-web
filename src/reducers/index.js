import { combineReducers } from 'redux';

import auth from './auth';
import drawer from './drawer';
import chat from './chat';
import { loadingBarReducer as loadingBar } from 'react-redux-loading-bar';
import modal from './modal';
import snack from './snack';
import trading from './trading';
import user from './user';
import ethereum from './ethereum';

export default combineReducers({
  auth,
  chat,
  drawer,
  ethereum,
  loadingBar,
  modal,
  snack,
  trading,
  user,
});

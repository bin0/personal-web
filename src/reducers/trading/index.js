import { combineReducers } from 'redux';

import { tradingReducer as trading } from 'routes/Trading';
import { equityReducer as equity } from 'routes/Trading';
import { tradesReducer as realTrades } from 'routes/Trading';
import {
  priceReducer as prices,
  positionReducer as positions,
} from 'routes/Trading/components/Positions';
import { reducer as trades } from 'routes/Trading/components/Trades';
import { reducer as balance } from 'routes/Trading/components/Positions/components/BalanceCards';

export default combineReducers({
  equity,
  trading,
  prices,
  positions,
  trades,
  balance,
  realTrades,
});

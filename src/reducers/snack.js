import createReducer from 'redux/create-reducer';
import { OPEN_SNACK, CLOSE_SNACK } from 'action-types';

const openSnack =
  ({ message = '', snackProps = {} }) =>
  (dispatch) => {
    dispatch({ type: OPEN_SNACK.SET, payload: { message, ...snackProps } });
  };
const closeSnack = () => (dispatch) => {
  dispatch({ type: CLOSE_SNACK.SET });
};

export const actions = {
  openSnack,
  closeSnack,
};

const INITIAL_STATE = {
  message: '',
  action: null,
  open: false,
};

const ACTION_HANDLER = {
  [OPEN_SNACK.SET]: (_, { payload }) => ({ ...payload, open: true }),
  [CLOSE_SNACK.SET]: () => INITIAL_STATE,
};

export default createReducer(INITIAL_STATE, ACTION_HANDLER);

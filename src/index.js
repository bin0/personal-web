import React, { lazy, Suspense } from 'react';
import ReactDOM from 'react-dom';
import { LoadingBar } from 'react-redux-loading-bar';
import { ALREADY_JOINED, ENTRYPOINT, LOGO, FALLBACK, BUTTON, DIV } from './constants';

const App = lazy(() => import('containers/App'));

const alreadyJoined = localStorage.getItem(ALREADY_JOINED);

const initApp = () => {
  if (!alreadyJoined) localStorage.setItem(ALREADY_JOINED, Date.now());

  ReactDOM.render(
    <Suspense fallback={<LoadingBar />}>
      <App />
    </Suspense>,
    document.getElementById(ENTRYPOINT)
  );
};

if (alreadyJoined) {
  initApp();
} else {
  const loadingLogo = document.getElementById(LOGO);
  const fallback = document.getElementById(FALLBACK);
  const accessBtn = document.createElement(BUTTON);
  const animationDiv = document.createElement(DIV);

  loadingLogo.style.display = 'none';
  accessBtn.className = 'btn';
  accessBtn.textContent = 'Accede ahora';
  accessBtn.appendChild(animationDiv);
  fallback.appendChild(accessBtn);
  animationDiv.className = 'btn-animated';
  accessBtn.addEventListener('click', initApp);
}

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import withStyles from '@mui/styles/withStyles';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import { Typography, Button } from '@mui/material';
import CloseIcon from '@mui/icons-material/Close';
import { customDate } from 'utils/dates';
import styles from '../routes/Trading/styles/trading';

class PositionTable extends Component {
  handleClose = () => {
    this.props.onExitPosition(this.props.market);
  };

  render() {
    const isAdmin = Boolean(JSON.parse(localStorage.getItem('isAdmin')));
    const { classes, positions, market } = this.props;
    return (
      <Paper className={classes.table.root}>
        <div style={{ margin: '1.5rem 1rem' }}>
          <Typography variant="h2">{market}</Typography>
        </div>
        <Table className={classes.table.table}>
          <TableHead>
            <TableRow>
              <TableCell>Fecha</TableCell>
              <TableCell align="right">Mercado</TableCell>
              <TableCell align="right">Precio Entrada</TableCell>
              <TableCell align="right">Dirección</TableCell>
              <TableCell align="right">Cantidad</TableCell>
              {isAdmin && <TableCell align="right">Cerrar</TableCell>}
            </TableRow>
          </TableHead>
          <TableBody>
            {positions.length &&
              positions.map((position) => (
                <TableRow key={position._id}>
                  <TableCell className={classes.cell}>
                    {customDate(position.startDate)}
                  </TableCell>
                  <TableCell className={classes.cell} align="right">
                    {position.market}
                  </TableCell>
                  <TableCell className={classes.cell} align="right">
                    {position.enterPrice}
                  </TableCell>
                  <TableCell className={classes.cell} align="right">
                    {position.direction}
                  </TableCell>
                  <TableCell className={classes.cell} align="right">
                    {position.quantity}
                  </TableCell>
                  {isAdmin && (
                    <TableCell align="right">
                      <Button
                        onClick={this.handleClose}
                        variant="contained"
                        color="primary"
                      >
                        <CloseIcon />
                      </Button>
                    </TableCell>
                  )}
                </TableRow>
              ))}
          </TableBody>
        </Table>
      </Paper>
    );
  }
}

PositionTable.propTypes = {
  classes: PropTypes.object.isRequired,
  positions: PropTypes.array.isRequired,
};

export default withStyles(styles)(PositionTable);

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { actions } from 'reducers/modal';
import MODAL_COMPONENTS from './modal-components';

const ModalRoot = ({ modalType, modalProps, open, closeModal: close }) => {
  const SpecificModal = MODAL_COMPONENTS[modalType];

  return (
    modalType &&
    SpecificModal && <SpecificModal open={open} close={close} {...modalProps} />
  );
};

ModalRoot.propTypes = {
  modalType: PropTypes.string,
  modalProps: PropTypes.object,
  open: PropTypes.bool.isRequired,
};

const mapStateToProps = ({ modal }) => ({
  modalType: modal.modalType,
  modalProps: modal.modalProps,
  open: modal.open,
});

const mapDispatchToProps = { ...actions };

export default connect(mapStateToProps, mapDispatchToProps)(ModalRoot);

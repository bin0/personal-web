import tradingModals from '../../routes/Trading/modals';

const modals = {
  ...tradingModals,
};

export default modals;

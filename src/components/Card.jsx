import React from 'react';
import PropTypes from 'prop-types';
import { useHistory } from 'react-router-dom';
import Avatar from '@mui/material/Avatar';
import CardHeader from '@mui/material/CardHeader';
import {
  CardContainerStyled,
  DobleArrowIconStyled,
} from 'routes/Developer/styles';
import { useCallback } from 'react';

const Card = ({ title, subheader, route }) => {
  const history = useHistory();

  const handleClick = useCallback(() => {
    history.push(`/developer/${route}/${title.toLowerCase()}`);
  }, [history, route, title]);

  return (
    <CardContainerStyled>
      <CardHeader
        avatar={<Avatar />}
        action={<DobleArrowIconStyled onClick={handleClick} />}
        title={title}
        subheader={subheader}
      />
    </CardContainerStyled>
  );
};

Card.propTypes = {
  title: PropTypes.string,
  subheader: PropTypes.string,
};

export default Card;

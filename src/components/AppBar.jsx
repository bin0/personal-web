import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import Avatar from '@mui/material/Avatar';
import AppBar from '@mui/material/AppBar';
import Slide from '@mui/material/Slide';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import IconButton from '@mui/material/IconButton';
import useScrollTrigger from '@mui/material/useScrollTrigger';
import { withStyles } from '@mui/styles';
import { actions as drawerActions } from 'reducers/drawer';
import { actions as modalActions } from 'reducers/modal';
import LogoAppBar from './LogoAppBar';
import Alerts from './Alerts';
import styles, { SubtitleStyled, AppBarContainerStyled } from './styles';
import './styles/firebaseui-styling.global.css';

const MyAppBar = ({
  authenticated,
  openDrawer,
  openModal,
  title,
  subtitle,
  color,
  classes,
  user,
  WithMenu,
  history,
  alerts,
}) => {
  const handleClickMenu = () => {
    if (authenticated) {
      openDrawer('TRADING_MENU');
      return;
    }
    openModal({ modalType: 'ADD_USER', modalProps: {} });
  };
  const trigger = useScrollTrigger({ threshold: 20 });

  return (
    <AppBarContainerStyled>
      <Slide appear={false} direction="down" in={!trigger}>
        <AppBar color={color}>
          <Toolbar className={classes.toolbar}>
            <div className="appbar-logo-container">
              <Link to="/">
                <LogoAppBar />
              </Link>
              <Typography
                variant="h6"
                color="inherit"
                className={classes.grow}
                onClick={() => history.push('/')}
              >
                {title}
                <SubtitleStyled>{subtitle}</SubtitleStyled>
              </Typography>
            </div>
            <div className="app-logo-actions">
              {alerts && <Alerts />}
              {WithMenu && <WithMenu />}
              {authenticated && (
                <IconButton
                  className={classes.menuButton}
                  onClick={handleClickMenu}
                  color="inherit"
                  aria-label="Menu"
                  size="large"
                >
                  <Avatar src={user.photoURL} alt="user-profile-img" />
                </IconButton>
              )}
            </div>
          </Toolbar>
        </AppBar>
      </Slide>
      <Toolbar />
    </AppBarContainerStyled>
  );
};

MyAppBar.defaultProps = {
  title: '',
  subtitle: '',
  color: 'primary',
  alerts: false,
};

MyAppBar.propTypes = {
  title: PropTypes.string,
  subtitle: PropTypes.string,
  color: PropTypes.string,
  classes: PropTypes.object.isRequired,
  user: PropTypes.object.isRequired,
  openDrawer: PropTypes.func.isRequired,
  closeDrawer: PropTypes.func.isRequired,
  authenticated: PropTypes.bool.isRequired,
  withMenu: PropTypes.element,
};

const StyledAppBar = withStyles(styles)(MyAppBar);

const mapStateToProps = ({ auth, user }) => ({
  authenticated: auth.authenticated,
  user,
});

const mapDispatchToProps = { ...drawerActions, ...modalActions };

export { MyAppBar as Testing };
export default connect(mapStateToProps, mapDispatchToProps)(StyledAppBar);

import React, { useState, useCallback } from 'react';
import { Link, withRouter } from 'react-router-dom';
import useMediaQuery from '@mui/material/useMediaQuery';
import MoreVertIcon from '@mui/icons-material/MoreVert';
import IconButton from '@mui/material/IconButton';
import Menu from '@mui/material/Menu';
import MenuItem from '@mui/material/MenuItem';
import { NavMenuContainerStyled } from './styles';

const NavMenu = ({ history: { push } }) => {
  const [anchorEl, setAnchorEl] = useState(null);
  const isMobile = useMediaQuery('(max-width:768px)');

  const handleClick = useCallback((event) => {
    setAnchorEl(event.currentTarget);
  }, []);

  const handleClose = useCallback(() => {
    setAnchorEl(null);
  }, []);

  const handleMenuClick = useCallback(
    (route) => () => {
      push(route);
      setAnchorEl(null);
    },
    [push]
  );

  return (
    <NavMenuContainerStyled isMobile={isMobile}>
      {isMobile ? (
        <>
          <Menu
            id="nav-options-menu"
            anchorEl={anchorEl}
            keepMounted
            open={Boolean(anchorEl)}
            onClose={handleClose}
          >
            <MenuItem onClick={handleMenuClick('/')}>Home</MenuItem>
            <MenuItem onClick={handleMenuClick('/blockchain')}>
              Blockchain
            </MenuItem>
            <MenuItem onClick={handleMenuClick('/web3')}>Web3</MenuItem>
            <MenuItem onClick={handleMenuClick('/developer')}>
              Developer
            </MenuItem>
            <MenuItem onClick={handleMenuClick('/trading')}>Trading</MenuItem>
          </Menu>
          <IconButton
            onClick={handleClick}
            color="inherit"
            aria-label="Menu"
            size="large"
          >
            <MoreVertIcon alt="user-profile" />
          </IconButton>
        </>
      ) : (
        <>
          <Link to="/">Home</Link>
          <Link to="/blockchain">Blockchain</Link>
          <Link to="/web3">Web3</Link>
          <Link to="/developer">Developer</Link>
          <Link to="/trading">Trading</Link>
        </>
      )}
    </NavMenuContainerStyled>
  );
};

export default withRouter(NavMenu);

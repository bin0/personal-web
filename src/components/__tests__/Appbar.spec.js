import React from 'react';
import { mount } from 'enzyme';
import { Router } from 'react-router-dom';
import { Testing as Appbar } from '../AppBar';
import { createMemoryHistory } from 'history';

describe('<Appbar />', () => {
  const history = createMemoryHistory();
  const props = {
    classes: {
      close: '',
    },
    user: {
      photoURL: 'url',
    },
    openModal: jest.fn(),
    openDrawer: jest.fn(),
    closeDrawer: jest.fn(),
    authenticated: true,
    displayName: 'Test name',
    WithMenu: () => <nav>menu</nav>,
  };

  it('should open drawer when authenticated', () => {
    const renderWrapper = mount(
      <Router history={history}>
        <Appbar {...props} history={history} />
      </Router>
    );

    renderWrapper.find('button').simulate('click');

    expect(props.openDrawer).toHaveBeenCalled();
  });

  it('should open add user modal when no authenticated', () => {
    props.authenticated = false;
    const renderWrapper = mount(
      <Router history={history}>
        <Appbar {...props} history={history} />
      </Router>
    );

    renderWrapper.find('button').simulate('click');

    expect(props.openModal).toHaveBeenCalled();
  });

  it('should handle click event over h6', () => {
    const renderWrapper = mount(
      <Router history={history}>
        <Appbar {...props} history={history} />
      </Router>
    );

    renderWrapper.find('h6').simulate('click');

    expect(history.length).toBe(2);
  });
});

import React from 'react';
import { mount, shallow } from 'enzyme';
import Card from '../Card';
import { DobleArrowIconStyled } from 'routes/Developer/styles';
import toJson from 'enzyme-to-json';

jest.mock('react-router-dom', () => ({
  useHistory: jest.fn().mockReturnValue({
    push: jest.fn(),
  }),
  Link: jest.fn(),
}));

describe('<Card />', () => {
  const props = {
    title: 'test title',
    subheader: 'test subheader',
    route: '/test',
  };

  describe('when passing props', () => {
    test('should match snapshot', () => {
      const wrapper = shallow(<Card {...props} />);

      expect(toJson(wrapper)).toMatchSnapshot();
    });
  });

  describe('when click action', () => {
    test('should handle onClick', () => {
      const wrapper = mount(<Card {...props} />);

      const instance = wrapper.find(DobleArrowIconStyled);
      instance.simulate('click');

      expect(instance).toHaveLength(1);
    });
  });
});

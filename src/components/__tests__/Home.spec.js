import React from 'react';
import { mount } from 'enzyme';
import toJSON from 'enzyme-to-json';
import { Testing as Home } from '../Home';

jest.mock('react-redux', () => ({
  useDispatch: jest.fn().mockReturnValue(jest.fn()),
}));

describe('<Home />', () => {
  it('should match snapshot', () => {
    const renderWrapper = mount(<Home />);
    expect(toJSON(renderWrapper)).toMatchSnapshot();
  });
});

import React from 'react';
import { MemoryRouter as Router } from 'react-router-dom';
import { mount } from 'enzyme';
import toJSON from 'enzyme-to-json';
import { Testing as Drawer } from '../Drawer';
import ExitIcon from '@mui/icons-material/ExitToApp';
import ChatIconOpen from '@mui/icons-material/Chat';
import ChatIconClose from '@mui/icons-material/ChatBubble';

describe('<Drawer />', () => {
  const props = {
    open: false,
    user: {},
    chatActive: false,
    logOut: jest.fn(),
    closeDrawer: jest.fn(),
    openDrawer: jest.fn(),
    onOpen: jest.fn(),
    toogleChat: jest.fn(),
  };

  test('should render correctly', () => {
    const wrapper = mount(<Drawer {...props} />);
    expect(toJSON(wrapper)).toMatchSnapshot();
  });
  test('should handle exit app', () => {
    props.open = true;
    const wrapper = mount(
      <Router initialEntries={['/']}>
        <Drawer {...props} />
      </Router>
    );

    const instance = wrapper.find(ExitIcon);
    instance.simulate('click');

    expect(props.logOut).toHaveBeenCalled();
  });

  test('should handle open chat', () => {
    props.open = true;
    props.chatActive = false;
    const wrapper = mount(
      <Router initialEntries={['/']}>
        <Drawer {...props} />
      </Router>
    );

    const instance = wrapper.find(ChatIconOpen);
    instance.simulate('click');

    expect(props.toogleChat).toHaveBeenCalled();
    expect(props.toogleChat).toHaveBeenCalledWith(!props.chatActive);
  });
  test('should handle close chat', () => {
    props.open = true;
    props.chatActive = true;
    const wrapper = mount(
      <Router initialEntries={['/']}>
        <Drawer {...props} />
      </Router>
    );

    const instance = wrapper.find(ChatIconClose);
    instance.simulate('click');

    expect(props.toogleChat).toHaveBeenCalled();
    expect(props.toogleChat).toHaveBeenCalledWith(!props.chatActive);
  });
});

import React from 'react';
import Contacto from '../Contacto';
import { mount } from 'enzyme';
import toJSON from 'enzyme-to-json';

describe('<Contacto />', () => {
  test('should render correctly', () => {
    const wrapper = mount(<Contacto />);
    expect(toJSON(wrapper)).toMatchSnapshot();
  });
});

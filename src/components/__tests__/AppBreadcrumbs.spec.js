import React from 'react';
import { Typography } from '@mui/material';
import { shallow } from 'enzyme';
import { LinkStyled } from 'routes/Developer/styles';
import AppBreadcrumbs from '../AppBreadcrumbs';

describe('<AppBreadcrumbs />', () => {
  it('should show the right path', () => {
    const renderWrapper = shallow(
      <AppBreadcrumbs pathname="/developer/frontend" />
    );

    const links = renderWrapper.find(LinkStyled);
    const current = renderWrapper.find(Typography);

    expect(links).toHaveLength(2);
    expect(current).toHaveLength(1);
  });
});

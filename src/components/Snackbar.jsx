import React, { useCallback } from 'react';
import PropTypes from 'prop-types';
import { Snackbar, IconButton, Alert, Typography } from '@mui/material';
import CloseIcon from '@mui/icons-material/Close';
import { connect } from 'react-redux';
import { actions } from 'reducers/snack';
import { Box } from '@mui/system';

const AppSnackbar = ({ closeSnack, duration, light, snack }) => {
  const handleClose = useCallback(
    (event, reason) => {
      if (reason === 'clickaway') {
        return;
      }
      closeSnack();
    },
    [closeSnack]
  );

  return (
    <Snackbar
      anchorOrigin={{
        vertical: 'top',
        horizontal: 'right',
      }}
      open={snack.open}
      autoHideDuration={duration}
      onClose={handleClose}
      action={[
        <IconButton
          key="close"
          aria-label="Close"
          color="inherit"
          onClick={handleClose}
          // size="large"
        >
          <CloseIcon />
        </IconButton>,
      ]}
    >
      <Alert
        onClose={handleClose}
        severity={snack.severity}
        sx={{ width: '100%' }}
      >
        <Box sx={{ alignItems: 'flex-end', height: '100%', display: 'flex' }}>
          <Typography variant="h4">{snack.message}</Typography>
        </Box>
      </Alert>
    </Snackbar>
  );
};

AppSnackbar.defaultProps = {
  light: true,
  duration: 6000,
  severity: 'info',
};

AppSnackbar.propTypes = {
  snack: PropTypes.shape({
    open: PropTypes.bool.isRequired,
    message: PropTypes.string.isRequired,
  }),
  openSnack: PropTypes.func.isRequired,
  closeSnack: PropTypes.func.isRequired,
  light: PropTypes.bool,
  duration: PropTypes.number,
};

const mapStateToProps = ({ snack }) => ({ snack });

const mapDispatchToProps = { ...actions };

export default connect(mapStateToProps, mapDispatchToProps)(AppSnackbar);

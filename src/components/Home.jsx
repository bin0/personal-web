import React, { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import Analitycs from 'utils/analitycs/google-analitycs';
import { actions } from 'reducers/snack';
import { isFirstVisit } from 'utils/session';
import AppLogo from './AppLogo';
import Contacto from './Contacto';
import './styles/styles.scss';

const Home = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    Analitycs.pageview('/', 'Home');
    Analitycs.event({
      category: 'User',
      action: 'Navigates to home',
      value: 0,
      label: 'Visited HomePage',
      nonInteraction: true,
    });

    isFirstVisit(() => {
      dispatch(actions.openSnack({ message: '¡ Bienvenido !' }));
    });
  }, [dispatch]);

  return (
    <div className="home-view-container">
      <Contacto />
      <AppLogo />
    </div>
  );
};

export { Home as Testing };
export default Home;

import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import withStyles from '@mui/styles/withStyles';
import SwipeableDrawer from '@mui/material/SwipeableDrawer';
import List from '@mui/material/List';
import Divider from '@mui/material/Divider';
import ListItem from '@mui/material/ListItem';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import MailIcon from '@mui/icons-material/Mail';
import ExitIcon from '@mui/icons-material/ExitToApp';
import AssignmentIcon from '@mui/icons-material/Assignment';
import ChatIconClose from '@mui/icons-material/ChatBubble';
import ChatIconOpen from '@mui/icons-material/Chat';
import AccountBalanceIcon from '@mui/icons-material/AccountBalance';
import Avatar from '@mui/material/Avatar';
import SettingsIcon from '@mui/icons-material/Settings';
import HomeIcon from '@mui/icons-material/Home';

import { actions as drawerActions } from '../reducers/drawer';
import { actions as authActions } from '../reducers/auth';
import { actions as chatActions } from '../reducers/chat';

import styles from './styles';

class TradingDrawer extends React.Component {
  static propTypes = {
    classes: PropTypes.object.isRequired,
    user: PropTypes.object.isRequired,
    chatActive: PropTypes.bool.isRequired,
    logOut: PropTypes.func.isRequired,
    closeDrawer: PropTypes.func.isRequired,
    openDrawer: PropTypes.func.isRequired,
  };

  handleExitApp = () => {
    this.props.logOut();
  };

  toogleChat = () => {
    this.props.toogleChat(!this.props.chatActive);
  };

  render() {
    const { classes, closeDrawer, openDrawer, user, chatActive } = this.props;

    const sideList = (
      <div className={classes.list}>
        <List>
          <ListItem button>
            <Avatar src={user.photoURL} />
            <ListItemText primary={user.displayName} />
          </ListItem>
        </List>
        <Divider />
        <List>
          {['Posiciones', 'Histórico'].map((text, index) => (
            <ListItem button key={text}>
              <ListItemIcon>
                {index % 2 === 0 ? <AccountBalanceIcon /> : <AssignmentIcon />}
              </ListItemIcon>
              <ListItemText primary={text} />
              <SettingsIcon />
            </ListItem>
          ))}
        </List>
        <Divider />
        <List>
          <Link to="/" className={classes.link}>
            <ListItem button>
              <ListItemIcon>
                <HomeIcon />
              </ListItemIcon>
              <ListItemText primary="Home" />
            </ListItem>
          </Link>
          <Link to="/portfolio" className={classes.link}>
            <ListItem button>
              <ListItemIcon>
                <MailIcon />
              </ListItemIcon>
              <ListItemText primary="PortFolio" />
            </ListItem>
          </Link>
          <ListItem button onClick={this.toogleChat}>
            <ListItemIcon>
              {chatActive ? <ChatIconClose /> : <ChatIconOpen />}
            </ListItemIcon>
            <ListItemText primary="Chat" />
          </ListItem>
        </List>
        <Divider />
        <List>
          <ListItem button onClick={this.handleExitApp}>
            <ListItemIcon>
              <ExitIcon />
            </ListItemIcon>
            <ListItemText primary="Salir" />
          </ListItem>
        </List>
      </div>
    );

    const iOS =
      typeof navigator !== 'undefined' &&
      /iPad|iPhone|iPod/.test(navigator.userAgent);

    return (
      <div>
        <SwipeableDrawer
          disableBackdropTransition={!iOS}
          disableDiscovery={iOS}
          anchor="right"
          open={this.props.open}
          onOpen={openDrawer}
          onClose={closeDrawer}
        >
          <div
            tabIndex={0}
            role="button"
            onClick={closeDrawer}
            onKeyDown={closeDrawer}
          >
            {sideList}
          </div>
        </SwipeableDrawer>
      </div>
    );
  }
}

const TradingDrawerWrapped = withStyles(styles)(TradingDrawer);

const mapStateToProps = ({ drawer, user, chat }) => ({
  open: drawer.open,
  chatActive: chat.chatActive,
  user,
});

const mapDispatchToProps = { ...drawerActions, ...authActions, ...chatActions };

export { TradingDrawerWrapped as Testing };
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TradingDrawerWrapped);

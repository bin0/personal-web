import React, { useCallback, useState } from 'react';
import NotificationsIcon from '@mui/icons-material/Notifications';
import NotificationsActiveIcon from '@mui/icons-material/NotificationsActive';

const Alerts = () => {
  const isActive = localStorage.getItem('ao-notifications-active');
  const [active, setActive] = useState(isActive || false);
  const handleNotificationsClick = useCallback(() => {
    setActive((state) => !state);
  }, []);
  return (
    <div className="alerts-container" onClick={handleNotificationsClick}>
      {active ? <NotificationsActiveIcon /> : <NotificationsIcon />}
    </div>
  );
};

export default Alerts;

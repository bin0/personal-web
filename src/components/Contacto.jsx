import React from 'react';
import { ContactoStyled } from './styles/contacto';

const Contacto = () => (
  <ContactoStyled>
    <a href="mailto:adolfo@onrubia.es">Contacto</a>
  </ContactoStyled>
);

export default Contacto;

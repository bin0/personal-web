import React from 'react';
import {
  // Typography,
  Link,
} from '@mui/material';
import withStyles from '@mui/styles/withStyles';
import { currentYear } from 'utils/dates';
import styles, {
  // FooterColumns,
  FooterContainer,
  FooterHeader,
  FooterSocials,
} from './styles';
import logoYouTube from '../assets/img/icono-youtube.png';
import logoTwitter from '../assets/img/icono-twitter.png';
import logoProrealcode from '../assets/img/prorealcode.jpg';

const footerLinks = {
  youtube:
    'https://www.youtube.com/channel/UCXA7Xl1fJ8oErsEg0UNUPRQ?view_as=subscriber',
  prorealcode: 'https://www.prorealcode.com/user/adolfo_onrubia',
};

const Footer = ({ classes }) => {
  return (
    <FooterContainer>
      <FooterSocials>
        <h5 className={classes.h5}>Sígueme en</h5>

        <Link
          to={footerLinks.youtube}
          href={footerLinks.youtube}
          underline="hover"
        >
          <img src={logoYouTube} alt="logo-youtube" />
        </Link>
        <img src={logoTwitter} alt="logo-twitter" />
        <Link
          to={footerLinks.prorealcode}
          href={footerLinks.prorealcode}
          target="_blank"
          underline="hover"
        >
          <img src={logoProrealcode} alt="logo-prorealcode" />
        </Link>
      </FooterSocials>
      <FooterHeader>
        &#169; Copyright {currentYear} | Adolfo Onrubia
      </FooterHeader>

      {/* <FooterColumns>
        <div className={classes.footerItem}>
          <Typography color="secondary" variant="h6">
            Enlaces
          </Typography>
          <ul>
            <li>Enlace 1</li>
            <li>Enlace 2</li>
            <li>Enlace 3</li>
            <li>Enlace 4</li>
          </ul>
        </div>
        <div className={classes.footerItem}>
          <ul>
            <li>Enlace 1</li>
            <li>Enlace 2</li>
            <li>Enlace 3</li>
            <li>Enlace 4</li>
          </ul>
        </div>
        <div className={classes.footerItem}>
          <ul>
            <li>Enlace 1</li>
            <li>Enlace 2</li>
            <li>Enlace 3</li>
            <li>Enlace 4</li>
          </ul>
        </div>
      </FooterColumns> */}
    </FooterContainer>
  );
};

export default withStyles(styles)(Footer);

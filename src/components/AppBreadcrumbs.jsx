import React, { useMemo } from 'react';
import PropTypes from 'prop-types';
import Breadcrumbs from '@mui/material/Breadcrumbs';
import Typography from '@mui/material/Typography';
import NavigateNextIcon from '@mui/icons-material/NavigateNext';
import { isLastItem } from 'utils/arrays';
import { getLink, getBreadcrumbFromPathname } from 'utils/strings';
import { LinkStyled } from 'routes/Developer/styles';
import { BreadcrumbsContainerStyled } from './styles/navigation';

const AppBreadcrumbs = ({ pathname }) => {
  const routes = useMemo(() => getBreadcrumbFromPathname(pathname), [pathname]);

  const renderRoutes = useMemo(
    () =>
      routes.map((route, i) =>
        isLastItem(routes, i) ? (
          <Typography key={route} variant="body2">
            {route}
          </Typography>
        ) : (
          <LinkStyled key={route} to={getLink(routes, route, i)}>
            {route}
          </LinkStyled>
        )
      ),
    [routes]
  );

  return (
    <BreadcrumbsContainerStyled>
      <Breadcrumbs
        separator={<NavigateNextIcon fontSize="small" />}
        aria-label="breadcrumb"
      >
        <LinkStyled to="/">Home</LinkStyled>
        {renderRoutes}
      </Breadcrumbs>
    </BreadcrumbsContainerStyled>
  );
};

AppBreadcrumbs.propTypes = {
  pathname: PropTypes.string.isRequired,
};

export default AppBreadcrumbs;

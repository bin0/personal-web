import styled from 'styled-components';

const theme = (theme) => ({
  grow: {
    flewGrow: 1,
    lineHeight: '30px',
    marginTop: '-6px',
    '@media (max-width: 768px)': {
      lineHeight: '20px',
    },
  },
  list: {
    width: 250,
  },
  fullList: {
    width: 'auto',
  },
  footerItem: {
    // width: '33%',
    border: '1px solid red',
    padding: theme.spacing(1),
  },
  h5: {},
  menuButton: {
    padding: 0,
    marginLeft: theme.spacing(2),
  },
  link: {
    textDecoration: 'none',
  },
  toolbar: {
    display: 'flex',
    justifyContent: 'space-between',
    paddingRight: 0,
  },
});

export const SeparatorStyled = styled.div`
  border-bottom: 1px solid #8ee7ff;
  height: 1px;
  margin: 1rem 0;
  padding: 1rem;
  width: 100%;

  @media (max-width: 768px) {
    padding: 0.5rem;
  }
`;

export const NavMenuContainerStyled = styled.div`
  display: flex;
  font-size: 0.9rem;
  justify-content: flex-end;
  margin: 0 0 0 1rem;

  p,
  a {
    color: #ccc;
    font-size: 0.8rem;
    font-weight: bold;
    margin-right: 1rem;
    text-decoration: none;
    letter-spacing: 0.025rem;
    text-shadow: 0.5px 0.5px #255968;
    text-transform: uppercase;

    &:hover {
      color: #61dafb;
    }
  }
`;

export const Box = styled.div`
  margin-right: 1rem;
  div {
    align-items: center;
    border: 0.1rem solid #282c33;
    box-shadow: 0px 0px 5px 0.1px #8ee7ff;
    box-sizing: border-box;
    display: flex;
    justify-content: center;
  }
`;

const sizes = {
  xl: '45px',
  lg: '30px',
  md: '20px',
  sm: '10px',
};

export const Box1 = styled.div`
  /* width: ${sizes.xl};
  height: ${sizes.xl}; */
  border: 0.03rem solid #282c33;
  border-radius: 50%;
`;
export const Box2 = styled.div`
  /* width: ${sizes.lg};
  height: ${sizes.lg}; */
  border: 0.03rem solid #282c33;
  transform: rotate(45deg);
`;
export const Box3 = styled.div`
  border: 0.03rem solid #282c33;
  height: 20px;
  transform: rotate(30deg);
  width: 20px;
`;
export const Box4 = styled.div`
  border: 0.03rem solid #282c33;
  height: 10px;
  transform: rotate(15deg);
  width: 10px;
`;

export const SubtitleStyled = styled.span`
  display: block;
  font-size: 0.8rem;
  font-weight: 100;
  line-height: 0.4rem;
  text-transform: uppercase;
  @media (max-width: 768px) {
    font-size: 0.7rem;
    line-height: 0.7rem;
  }
`;

export const FooterColumns = styled.div`
  /* background: #8ee7ff; */
  display: flex;
  margin: 1rem;

  ul {
    list-style: none;
  }

  display: flex;
  justify-content: space-between;
`;

export const FooterContainer = styled.div`
  min-height: 100px;

  display: flex;
  flex-direction: column;
  justify-content: space-between;
`;

export const FooterHeader = styled.div`
  align-items: center;
  color: #8ee7ff;
  display: flex;
  font-size: 0.9rem;
  justify-content: center;
  margin: 1rem;
`;
export const FooterSocials = styled.div`
  display: flex;
  justify-content: space-around;
  margin: 1rem;
`;

export const FooterStyled = styled.footer`
  position: absolute;
  bottom: 0;
  right: 5px;

  a {
    color: #ccc;
    font-size: 0.9rem;
    font-weight: 500;

    &:hover {
      color: #8ee7ff;
    }
  }
`;

export const CircularProgressStyled = styled.div`
  align-items: center;
  display: flex;
  height: 100px;
  justify-content: center;
`;

export const AppBarContainerStyled = styled.div``;

export default theme;

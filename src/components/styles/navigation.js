import styled from 'styled-components';

export const NavigationContainerStyled = styled.div`
  display: flex;
  align-items: center;

  svg {
    cursor: pointer;
  }
`;

export const BreadcrumbsContainerStyled = styled.div`
  margin-left: 1rem;
`;

import React from 'react';
import { currentYear } from 'utils/dates';
import { FooterStyled } from './styles';

const Footer = () => (
  <FooterStyled>
    <a
      href="https://github.com/Binomi0/"
      alt="GitHub"
      target="_blank"
      rel="noopener noreferrer"
    >
      @Binomio {currentYear}
    </a>
  </FooterStyled>
);

export default Footer;

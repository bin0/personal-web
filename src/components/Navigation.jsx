import React, { useCallback } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import Back from '@mui/icons-material/KeyboardBackspace';
import Forward from '@mui/icons-material/Forward';
import { NavigationContainerStyled } from './styles/navigation';
import AppBreadcrumbs from './AppBreadcrumbs';

const Navigation = ({ back, forward, location: { pathname }, history }) => {
  const handleGoBack = useCallback(() => {
    history.goBack();
  }, [history]);

  return (
    <NavigationContainerStyled>
      {back && <Back color="secondary" onClick={handleGoBack} />}
      <AppBreadcrumbs pathname={pathname} />
      {forward && <Forward />}
    </NavigationContainerStyled>
  );
};

Navigation.defaultProps = {
  back: false,
  forward: false,
};

Navigation.propTypes = {
  back: PropTypes.bool,
  forward: PropTypes.bool,
  location: PropTypes.shape({
    pathname: PropTypes.string.isRequired,
  }),
  history: PropTypes.shape({
    goBack: PropTypes.func.isRequired,
  }),
};

export default withRouter(Navigation);

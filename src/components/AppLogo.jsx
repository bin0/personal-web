import React from 'react';
import { Zoom } from '@mui/material';

const AppLogo = () => (
  <Zoom in style={{ transitionDelay: '500ms' }} timeout={{ enter: 2000 }}>
    <div className="app-logo-container">
      <div className="app-logo logo1" />
      <div className="app-logo logo2" />
      <div className="app-logo logo3" />
      <div className="app-logo logo4" />
      <h4>Adolfo J. Onrubia Albalá</h4>
      <p>Full Stack Developer</p>
      <p>Entrepreneur</p>
      <p>Trader</p>
    </div>
  </Zoom>
);

export default AppLogo;

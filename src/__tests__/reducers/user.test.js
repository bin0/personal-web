import axios from 'axios';
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import { actions, deleteUser } from 'reducers/user';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);
jest.mock('axios');

describe('user reducer', () => {
  describe('setUser()', () => {
    describe('when it runs', () => {
      it('should set user', async () => {
        const expectedActions = [
          {
            type: '/user/SET_USER_SET',
            payload: 'user',
          },
        ];
        const store = mockStore({});

        store.dispatch(actions.setUser('user'));

        expect(store.getActions()).toEqual(expectedActions);
      });
    });
  });
  describe('deleteUser()', () => {
    describe('when it runs', () => {
      it('should delete a user', async () => {
        const expectedActions = [{ type: '/user/DELETE_USER_SET' }];
        const store = mockStore({});

        store.dispatch(deleteUser('user'));

        expect(store.getActions()).toEqual(expectedActions);
      });
    });
  });
  describe('getUser()', () => {
    describe('when it runs', () => {
      it('should get user', async () => {
        axios.get = jest
          .fn()
          .mockResolvedValue({ data: { email: 'test@email.com' } });
        const expectedActions = [{ type: '/user/GET_USER_REQUEST' }];
        const store = mockStore({});

        store.dispatch(actions.getUser({ email: 'test@email.com' }));

        expect(store.getActions()).toEqual(expectedActions);
      });
    });
  });
  describe('createNewUser()', () => {
    describe('when it runs', () => {
      it('should get user', async () => {
        axios.post = jest
          .fn()
          .mockResolvedValue({ data: { email: 'test@email.com' } });
        const expectedActions = [{ type: '/user/CREATE_NEW_USER_REQUEST' }];
        const store = mockStore({});

        store.dispatch(actions.createNewUser({ email: 'test@email.com' }));

        expect(store.getActions()).toEqual(expectedActions);
      });
    });
  });
  describe('setUserName()', () => {
    describe('when it runs', () => {
      it('should set user name', async () => {
        const expectedActions = [
          { type: '/user/SET_USER_NAME_SET', payload: 'username' },
        ];
        const store = mockStore({});

        store.dispatch(actions.setUserName('username'));

        expect(store.getActions()).toEqual(expectedActions);
      });
    });
  });

  describe('setTraidingAccountType()', () => {
    describe('when it runs', () => {
      it('should set user name', async () => {
        const expectedActions = [
          {
            type: '/user/SET_TRAIDING_ACCOUNT_TYPE_SET',
            payload: 'account type',
          },
        ];
        const store = mockStore({});

        store.dispatch(actions.setTraidingAccountType('account type'));

        expect(store.getActions()).toEqual(expectedActions);
      });
    });
  });
});

import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import { actions } from 'reducers/modal';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('drawer reducer', () => {
  describe('openModal()', () => {
    describe('when it runs', () => {
      it('should show modal', async () => {
        const expectedActions = [
          {
            type: '/modals/OPEN_MODAL_SET',
            payload: { modalType: 'test', modalProps: { foo: 'bar ' } },
          },
        ];
        const store = mockStore({});

        store.dispatch(
          actions.openModal({ modalType: 'test', modalProps: { foo: 'bar ' } })
        );

        expect(store.getActions()).toEqual(expectedActions);
      });
    });
  });
  describe('closeModal()', () => {
    describe('when it runs', () => {
      it('should hide modal', async () => {
        const expectedActions = [{ type: '/modals/CLOSE_MODAL_SET' }];
        const store = mockStore({});

        store.dispatch(actions.closeModal());

        expect(store.getActions()).toEqual(expectedActions);
      });
    });
  });
});

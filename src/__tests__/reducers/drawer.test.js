import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import { actions } from 'reducers/drawer';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('drawer reducer', () => {
  describe('openDrawer()', () => {
    describe('when it runs', () => {
      it('should show drawer', async () => {
        const expectedActions = [
          { type: '/drawer/OPEN_DRAWER_SET', payload: 'type' },
        ];
        const store = mockStore({});

        store.dispatch(actions.openDrawer('type'));

        expect(store.getActions()).toEqual(expectedActions);
      });
    });
  });
  describe('closeDrawer()', () => {
    describe('when it runs', () => {
      it('should hide drawer', async () => {
        const expectedActions = [{ type: '/drawer/CLOSE_DRAWER_SET' }];
        const store = mockStore({});

        store.dispatch(actions.closeDrawer());

        expect(store.getActions()).toEqual(expectedActions);
      });
    });
  });
});

import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import { actions } from 'reducers/auth';
import firebase from 'config/__mocks__/firebase';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('auth reducer', () => {
  describe('checkUser()', () => {
    describe('when it runs', () => {
      it('should get user or logout', async () => {
        const expectedActions = [
          { type: '/user/CHECK_USER_REQUEST' },
          { type: '/user/CHECK_USER_SUCCESS' },
          { type: '/user/LOG_IN_SUCCESS' },
          { type: '/user/GET_USER_REQUEST' },
        ];
        const store = mockStore({});

        store.dispatch(actions.checkUser());

        expect(store.getActions()).toEqual(expectedActions);
      });
    });
    describe('when it fails', () => {
      it('should get user or logout', async () => {
        firebase.auth().onAuthStateChanged((user) => {
          expect(user.providerData[0].user).toBe('test');
        });
      });
    });
  });
  describe('logOut()', () => {
    describe('when it runs', () => {
      it('should get user or logout', async () => {
        const expectedActions = [
          { type: '/user/LOG_OUT_SET' },
          { type: '/user/SET_USER_SET', payload: {} },
          { type: '/user/DELETE_USER_SET' },
        ];
        const store = mockStore({});

        store.dispatch(actions.logOut());

        expect(store.getActions()).toEqual(expectedActions);
      });
    });
  });
});

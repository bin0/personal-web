import React from 'react';
import { renderLazyComponent } from 'utils/components';
import { mount } from 'enzyme';

describe('Utils components', () => {
  describe('renderLazyComponent(Component, props)', () => {
    test('should return a component', () => {
      const C = () => <div>Test</div>;
      const wrapper = mount(renderLazyComponent(C));

      expect(wrapper.html()).toBe('<div>Test</div>');
    });
  });
});

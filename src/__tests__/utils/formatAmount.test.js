import formatter from 'utils/formatAmount';

describe('Utils currencies', () => {
  describe('formatter()', () => {
    test('shoul be shown 1 in euros format', () => {
      expect(formatter('en-US').format(1)).toBe('€1');
    });
    test('shoul be shown 1000 in euros format', () => {
      expect(formatter('en-US').format(1000)).toBe('€1,000');
    });
    test('shoul be shown 10.01 in euros format', () => {
      expect(formatter('en-US').format(10.01)).toBe('€10.01');
    });
    test('shoul be shown 0 in euros format', () => {
      expect(formatter('en-US').format(0)).toBe('€0');
    });
  });
});

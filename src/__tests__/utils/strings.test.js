import { capitalize, getLink } from 'utils/strings';

describe('Utils String', () => {
  describe('getLink(array, string, number)', () => {
    test('should return correct link', () => {
      expect(getLink(['a', 'b', 'c'], 'a', 0)).toBe('/a');
    });
    test('should return correct link when 1 deep', () => {
      expect(getLink(['a', 'b', 'c'], 'b', 1)).toBe('/a/b');
    });
  });

  describe('capitalize(string)', () => {
    test('should return capitalized string', () => {
      expect(capitalize('hola')).toBe('Hola');
    });
    test('should throw if no string passed', () => {
      expect(() => {
        capitalize();
      }).toThrowError(/Only/);
    });
    test('should throw if argument passed is a function', () => {
      expect(() => {
        const fn = () => {};
        capitalize(fn);
      }).toThrowError(/Only/);
    });
    test('should throw if argument passed is null', () => {
      expect(() => {
        capitalize(null);
      }).toThrowError(/Only/);
    });
    test('should throw if argument passed is an array', () => {
      expect(() => {
        capitalize([]);
      }).toThrowError(/Only/);
    });
    test('should throw if argument passed is an object', () => {
      expect(() => {
        capitalize({});
      }).toThrowError(/Only/);
    });
    test('should throw if argument passed is a number', () => {
      expect(() => {
        capitalize(7);
      }).toThrowError(/Only/);
    });
    test('should work if string length is 1', () => {
      expect(capitalize('a')).toBe('A');
    });
    test('should throw empty error if string length is 0', () => {
      expect(() => {
        capitalize('');
      }).toThrowError(/Empty/);
    });
  });
});

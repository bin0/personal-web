import { currentMonthText, currentYear, customDate } from 'utils/dates';

const months = {
  0: 'January',
  1: 'February',
  2: 'March',
  3: 'April',
  4: 'May',
  5: 'June',
  6: 'July',
  7: 'August',
  8: 'September',
  9: 'October',
  10: 'November',
  11: 'December',
};

describe('Utils dates', () => {
  describe('currentMonthText()', () => {
    test('should return current month in text format', () => {
      const month = new Date().getMonth();
      expect(currentMonthText('en-US')).toBe(months[month]);
    });
  });

  describe('currentYear()', () => {
    test('should return current month in text format', () => {
      expect(currentYear).toBe(2020);
    });
  });

  describe('customDate()', () => {
    test('should return current month in text format', () => {
      const testDate = '2020/04/20';
      expect(customDate(new Date(testDate))).toBe(
        'Abierta el 20/04/2020 a las 00:04:00'
      );
    });
  });
});

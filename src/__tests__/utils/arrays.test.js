import {
  isLastItem,
  reduceListByFields,
  orderObjectListByField,
} from 'utils/arrays';

describe('Utils Array', () => {
  describe('isLastItem(array, number)', () => {
    const list = [1, 2, 3, 4, 5, 6];
    test('should return true if is the last item in array', () =>
      expect(isLastItem(list, 5)).toBeTruthy());
    test("should return false if isn't the last item in array", () =>
      expect(isLastItem(list, 4)).toBeFalsy());
    test('should return false if index > array.length', () =>
      expect(isLastItem(list, 6)).toBeFalsy());
    test('should return false if index === 0', () =>
      expect(isLastItem(list, 0)).toBeFalsy());
    test('should return false if index < 0', () =>
      expect(isLastItem(list, -1)).toBeFalsy());
  });

  describe('reduceListByFields(...string[])([], Object)', () => {
    const list = [{ foo: 'bar', list: ['foo', 'bar'] }];

    test('should return a list of strings', () => {
      const result = ['bar', 'foo', 'bar'];
      expect(list.reduce(reduceListByFields('foo', 'list'), [])).toStrictEqual(
        result
      );
    });

    test('should return a list of strings 2', () => {
      const result = ['foo', 'bar'];
      expect(list.reduce(reduceListByFields('list'), [])).toStrictEqual(result);
    });

    test('should return an empty list', () => {
      const result = [];
      expect(list.reduce(reduceListByFields(), [])).toStrictEqual(result);
    });
  });

  describe('orderObjectListByField', () => {
    describe('when it runs', () => {
      it('should work', () => {
        const trades = [
          { createdAt: 'b' },
          { createdAt: 'a' },
          { createdAt: 'c' },
          { createdAt: 'c' },
        ];
        expect(
          trades.sort(orderObjectListByField('createdAt'))[0]
        ).toHaveProperty('createdAt', 'a');
      });
    });
  });
});

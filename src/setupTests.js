import React from 'react';
import Enzyme from 'enzyme';
import Adapter from '@wojtekmaj/enzyme-adapter-react-17';

React.useLayoutEffect = React.useEffect;

Enzyme.configure({ adapter: new Adapter() });

jest.mock('./utils/analitycs/google-analitycs');
jest.mock('./config/firebase');
jest.mock('react-firebaseui/StyledFirebaseAuth');
global.MutationObserver = class {
  disconnect() {}
  observe(element, initObject) {}
};

Object.defineProperty(window, 'scrollTo', {
  writable: true,
  value: jest.fn(),
});
